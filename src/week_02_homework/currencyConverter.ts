import { ResourceNotFoundError } from "./exceptions/ResourceNotFoundError";
import { UnknownError } from "./exceptions/UnknownError";
import { ValidationError } from "./exceptions/ValidationError";
import { IExchangeRateService } from "./exchangeRateService";

export class CurrencyConverter {

    private readonly FIXED_AMOUNT = 100;

    constructor(private exchangeRateService: IExchangeRateService) { }

    public Convert(amount: number, fromCurrency: string, toCurrency: string): number {
        this.validateAmount(amount);
        const exchangeRate = this.getExchangeRate(fromCurrency, toCurrency);
        this.validateExchangeRate(exchangeRate);
        return amount * exchangeRate;
    }

    public GenerateConversionReport(
        fromCurrency: string,
        toCurrency: string,
        startDate: Date,
        endDate: Date
    ): string {
        const conversions: number[] = [];

        const currentDate = new Date(startDate);

        while (currentDate <= endDate) {
            const exchangeRate = this.getExchangeRate(fromCurrency, toCurrency);
            this.validateExchangeRate(exchangeRate);
            this.calculateConversion(exchangeRate, conversions, currentDate);
        }

        return `Conversion Report:\n${conversions.join('\n')}`;
    }

    private getExchangeRate(fromCurrency: string, toCurrency: string): number {
        try {
            return this.exchangeRateService.getExchangeRate(fromCurrency, toCurrency);
        } catch (error) {
            if (error instanceof ResourceNotFoundError) {
                throw error;
            }

            throw new UnknownError('Unknown error happened.');
        }
    }

    private calculateConversion(exchangeRate: number, conversions: number[], currentDate: Date): void {
        const convertedAmount = this.FIXED_AMOUNT * exchangeRate; // Assume a fixed amount for simplicity
        conversions.push(convertedAmount);
        currentDate.setDate(currentDate.getDate() + 1);
    }

    private validateExchangeRate(exchangeRate: number): void {
        if (isNaN(exchangeRate)) {
            throw new ValidationError('Invalid exchange rate.');
        }
        
        if (!exchangeRate) {
            throw new ValidationError('Unable to fetch exchange rate.');
        }
    }

    private validateAmount(amount: number): void {
        if (isNaN(amount)) {
            throw new ValidationError('Invalid amount input.');
        }
    }
}