import { IDatabaseClient } from '../abstraction/clients/IDatabaseClient';
import { ICourseRepository } from '../abstraction/repository/ICourseRepository';
import { NetworkError } from '../exceptions/networkError';
import { UnknownError } from '../exceptions/unknownError';
import { Course } from '../models/course';
import { CourseStatistics } from '../models/courseStatistics';
import { Student } from '../models/student';

export class CourseRepository implements ICourseRepository {

    private static readonly ERROR_MESSAGE_DB_CLIENT_FAILED = 'DbClient failed.';

    constructor(private databaseClient: IDatabaseClient) {}

    public async addCourse(course: Course): Promise<void> {
        try {
            await this.databaseClient.insertCourse(course);
        } catch (error) {
            if (error instanceof NetworkError) {
                throw error;
            }

            throw new UnknownError(CourseRepository.ERROR_MESSAGE_DB_CLIENT_FAILED, error as Error);
        }
    }

    public async addStudentToCourse(student: Student, courseName: string): Promise<void> {
        try {
            await this.databaseClient.insertStudentToCourse(student, courseName);
        } catch (error) {
            if (error instanceof NetworkError) {
                throw error;
            }

            throw new UnknownError(CourseRepository.ERROR_MESSAGE_DB_CLIENT_FAILED, error as Error);
        }
    }

    public async getCourseByName(courseName: string): Promise<Course | undefined> {
        try {
            return await this.databaseClient.selectCourseByName(courseName);
        } catch (error) {
            if (error instanceof NetworkError) {
                throw error;
            }

            throw new UnknownError(CourseRepository.ERROR_MESSAGE_DB_CLIENT_FAILED, error as Error);
        }
    }

    public async getCourses(): Promise<Course[]> {
        try {
            return await this.databaseClient.selectAllCourses();
        } catch (error) {
            if (error instanceof NetworkError) {
                throw error;
            }

            throw new UnknownError(CourseRepository.ERROR_MESSAGE_DB_CLIENT_FAILED, error as Error);
        }
    }

    public async getCourseStatistics(courseName: string): Promise<CourseStatistics> {
        try {
            return await this.databaseClient.selectCourseStatisticsByName(courseName);
        } catch (error) {
            if (error instanceof NetworkError) {
                throw error;
            }

            throw new UnknownError(CourseRepository.ERROR_MESSAGE_DB_CLIENT_FAILED, error as Error);
        }
    }
    
}