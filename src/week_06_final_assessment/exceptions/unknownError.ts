import { Logger } from "./logger";

export class UnknownError extends Error {

    public constructor(message: string, private error?: Error) {
        super(message);
        this.name = this.constructor.name;

        Logger.logError(message, this.error);
    }

}