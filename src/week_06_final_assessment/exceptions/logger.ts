export class Logger {

    public static logError(errorMessage: string, errorObject?: Error) {
        console.log(errorMessage);
        
        if(errorObject) {
            console.log(JSON.stringify(errorObject));
        }
    }
    
}