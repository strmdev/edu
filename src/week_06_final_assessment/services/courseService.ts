import { Course } from '../models/course';
import { Student } from '../models/student';
import { CourseStatistics } from '../models/courseStatistics';
import { ICourseRepository } from '../abstraction/repository/ICourseRepository';
import { ICourseService } from '../abstraction/services/ICourseService';
import { IPaymentService } from '../abstraction/services/IPaymentService';
import { INotificationService } from '../abstraction/services/INotificationService';
import { ValidationError } from '../exceptions/validationError';


export class CourseService implements ICourseService {

    constructor(
        private courseRepository: ICourseRepository,
        private paymentService: IPaymentService,
        private notificationService: INotificationService
    ) { }

    public async addCourse(course: Course): Promise<void> {
        await this.courseRepository.addCourse(course);
    }

    public async getCourses(): Promise<Course[]> {
        return await this.courseRepository.getCourses();
    }

    public async addStudentToCourse(student: Student, courseName: string): Promise<void> {
        await this.validateCourseExists(courseName);
        await this.validatePaymentStatus(student);

        await this.courseRepository.addStudentToCourse(student, courseName);

        const notificationMessage = `${student.getName()} student was added to course.`;
        await this.notificationService.sendNotifications(notificationMessage);
    }

    public async getCourseStatistics(courseName: string): Promise<CourseStatistics> {
        await this.validateCourseExists(courseName);
        
        return await this.courseRepository.getCourseStatistics(courseName);
    }

    private async validateCourseExists(courseName: string): Promise<void> {
        const course = await this.courseRepository.getCourseByName(courseName);
        if (!course) {
            const errorMessage = 'Course not found';
            throw new ValidationError(errorMessage);
        }
    }

    private async validatePaymentStatus(student: Student): Promise<void> {
        const isCoursePayedByStudent = await this.paymentService.getIsOrderPayed(student);
        if (!isCoursePayedByStudent) {
            const errorMessage = 'Course is not yet paid by Student.';
            throw new ValidationError(errorMessage);
        }
    }

}