import { IMessageClient } from '../abstraction/clients/IMessageClient';
import { INotificationService } from '../abstraction/services/INotificationService';

export class NotificationService implements INotificationService {

    constructor(private messageClients: IMessageClient[]) { }

    public async sendNotifications(message: string): Promise<void> {
        this.messageClients.forEach(messageClient => {
            messageClient.sendNotification(message);
        });
    }

}