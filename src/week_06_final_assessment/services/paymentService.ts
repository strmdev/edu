import { IFinancialApiClient } from '../abstraction/clients/IFinancialApiClient';
import { IPaymentService } from '../abstraction/services/IPaymentService';
import { NetworkError } from '../exceptions/networkError';
import { ResourceNotFoundError } from '../exceptions/resourceNotFoundError';
import { UnknownError } from '../exceptions/unknownError';
import { Student } from '../models/student';

export class PaymentService implements IPaymentService {

    private static readonly DEFAULT_ERROR_MESSAGE = 'Unknown error happened.';

    constructor(private financialApiClient: IFinancialApiClient) { }

    public async getIsOrderPayed(student: Student): Promise<boolean> {
        try {
            return await this.financialApiClient.checkPaymentStatus(student);
        } catch (error) {
            if (error instanceof NetworkError || error instanceof ResourceNotFoundError) {
                throw error;
            }

            throw new UnknownError(PaymentService.DEFAULT_ERROR_MESSAGE, error as Error);
        }
    }
    
    public async makePaymentForCourse(student: Student): Promise<void> {
        try {
            await this.financialApiClient.makePayment(student);
        } catch (error) {
            if (error instanceof NetworkError || error instanceof ResourceNotFoundError) {
                throw error;
            }

            throw new UnknownError(PaymentService.DEFAULT_ERROR_MESSAGE, error as Error);
        }
    }

}