import { Course } from '../../models/course';
import { CourseStatistics } from '../../models/courseStatistics';
import { Student } from '../../models/student';

export interface IDatabaseClient {

    insertCourse(course: Course): Promise<void>;
    insertStudentToCourse(student: Student, courseName: string): Promise<void>;
    selectCourseByName(courseName: string): Promise<Course | undefined>;
    selectAllCourses(): Promise<Course[]>;
    selectCourseStatisticsByName(courseName: string): Promise<CourseStatistics>;

}