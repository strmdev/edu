import { Student } from '../../models/student';

export interface IFinancialApiClient {

    checkPaymentStatus(student: Student): Promise<boolean>;
    makePayment(student: Student): Promise<void>;

}