import { Course } from '../../models/course';
import { CourseStatistics } from '../../models/courseStatistics';
import { Student } from '../../models/student';

export interface ICourseService {

    addCourse(course: Course): Promise<void>;
    getCourses(): Promise<Course[]>;
    addStudentToCourse(student: Student, courseName: string): Promise<void>;
    getCourseStatistics(courseName: string): Promise<CourseStatistics>;

}