import { Student } from '../../models/student';

export interface IPaymentService {
    
    getIsOrderPayed(student: Student): Promise<boolean>;
    makePaymentForCourse(student: Student): Promise<void>;

}