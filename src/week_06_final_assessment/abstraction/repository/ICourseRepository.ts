import { Course } from '../../models/course';
import { CourseStatistics } from '../../models/courseStatistics';
import { Student } from '../../models/student';

export interface ICourseRepository {

    addCourse(course: Course): Promise<void>;
    addStudentToCourse(student: Student, courseName: string): Promise<void>;
    getCourseByName(courseName: string): Promise<Course | undefined>;
    getCourses(): Promise<Course[]>;
    getCourseStatistics(courseName: string): Promise<CourseStatistics>;

}