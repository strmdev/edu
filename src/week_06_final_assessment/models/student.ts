import { Course } from './course';
import { Person } from './person';

export class Student extends Person {
    
    constructor(name: string, emailAddress: string, phoneNumber: string, private registeredCourses: Course[] = []) {
        super(name, emailAddress, phoneNumber);
    }

    public getRegisteredCourses(): Course[] {
        return this.registeredCourses;
    }

}