export abstract class Person {

    constructor(
        protected name: string,
        protected emailAddress: string,
        protected phoneNumber: string
    ) { }

    public getName(): string {
        return this.name;
    }

    public getEmailAddress(): string {
        return this.emailAddress;
    }

    public getPhoneNumber(): string {
        return this.phoneNumber;
    }

}