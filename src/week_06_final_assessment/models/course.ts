import { Student } from './student';

export class Course {

    private students: Student[] = [];

    constructor(
        private courseName: string,
        private startDate: Date,
        private lengthInWeeks: number,
        private costInHuf: number
    ) { }

    public getCourseName(): string {
        return this.courseName;
    }

    public getStartDate(): Date {
        return this.startDate;
    }

    public getLengthInWeeks(): number {
        return this.lengthInWeeks;
    }

    public getCostInHuf(): number {
        return this.costInHuf;
    }

    public getStudentCount(): number {
        return this.students.length;
    }

    public addStudent(student: Student): void {
        this.students.push(student);
    }

    public getStudents(): Student[] {
        return this.students;
    }

}