export class CourseStatistics {
    
    constructor (
        private courseName: string,
        private totalLectures: number,
        private lecturesCompleted: number,
        private progress: number,
        private lastAccessed: Date
    ) { }

    public getCourseName(): string {
        return this.courseName;
    }

    public getTotalLectures(): number {
        return this.totalLectures;
    }

    public getLecturesCompleted(): number {
        return this.lecturesCompleted;
    }

    public getProgress(): number {
        return this.progress;
    }

    public getLastAccessed(): Date {
        return this.lastAccessed;
    }

}