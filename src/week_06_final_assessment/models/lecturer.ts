import { Course } from './course';
import { Person } from './person';

export class Lecturer extends Person {

    private assignedCourses: Course[] = [];

    constructor(name: string, emailAddress: string, phoneNumber: string) {
        super(name, emailAddress, phoneNumber);
    }

    public assignToCourse(course: Course): void {
        this.assignedCourses.push(course);
    }

    public getAssignedCourses(): Course[] {
        return this.assignedCourses;
    }

}