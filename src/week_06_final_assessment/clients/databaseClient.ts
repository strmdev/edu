import { IDatabaseClient } from '../abstraction/clients/IDatabaseClient';
import { Course } from '../models/course';
import { CourseStatistics } from '../models/courseStatistics';
import { Student } from '../models/student';
import { delay } from './common';

export class DatabaseClient implements IDatabaseClient{

    public async insertCourse(course: Course): Promise<void> {
        await delay(1500);
    }

    public async insertStudentToCourse(student: Student, courseName: string): Promise<void> {
        await delay(1500);
    }

    public async selectCourseByName(courseName: string): Promise<Course | undefined> {
        const cleanCodeCourse = new Course(
            'Clean Code',
            new Date(2024, 6, 1),
            6,
            100_000
        );

        await delay(1500);
        return cleanCodeCourse;
    }

    public async selectAllCourses(): Promise<Course[]> {
        const cleanCodeCourse = new Course(
            'Clean Code',
            new Date(2024, 6, 1),
            6,
            100_000
        );
        const unitTestingCourse = new Course(
            'Unit testing',
            new Date(2024, 7, 1),
            7,
            200_000
        );
        
        await delay(1500);
        return [cleanCodeCourse, unitTestingCourse];
    }
    
    public async selectCourseStatisticsByName(courseName: string): Promise<CourseStatistics> {
        const cleanCodeCourseStatistic = new CourseStatistics(
            courseName,
            60,
            30,
            50,
            new Date(2024, 4, 29)
        );

        await delay(1500);
        return cleanCodeCourseStatistic;
    }

}