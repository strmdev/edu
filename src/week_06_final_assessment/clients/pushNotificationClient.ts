import { IMessageClient } from '../abstraction/clients/IMessageClient';
import { delay } from './common';

export class PushNotificationClient implements IMessageClient {

    constructor(private deviceToken: string) { }

    public async sendNotification(message: string): Promise<void> {
        console.log(`Sending push notification to ${this.deviceToken}: ${message}`);
        await delay(1500);
    }
    
}