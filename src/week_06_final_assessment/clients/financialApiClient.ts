import { IFinancialApiClient } from '../abstraction/clients/IFinancialApiClient';
import { ResourceNotFoundError } from '../exceptions/resourceNotFoundError';
import { Student } from '../models/student';
import { delay } from './common';

export class FinancialApiClient implements IFinancialApiClient {
    
    public async makePayment(student: Student): Promise<void> {
        if(student.getName() === '') {
            const errorMessage = 'Resource not found';
            throw new ResourceNotFoundError(errorMessage);
        }

        await delay(1500);
    }
    
    public async checkPaymentStatus(student: Student): Promise<boolean> {
        if(student.getName() === '') {
            return false;
        }

        await delay(1500);
        return true;
    }

}