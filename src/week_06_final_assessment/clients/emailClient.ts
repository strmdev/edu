import { IMessageClient } from '../abstraction/clients/IMessageClient';
import { delay } from './common';

export class EmailClient implements IMessageClient {

    constructor(
        private sender: string,
        private receiver: string,
        private subject: string
    ) { }

    public async sendNotification(message: string): Promise<void> {
        console.log(`Sending email from ${this.sender} to ${this.receiver} with this subject (${this.subject}) and message (${message})`);
        await delay(1500);
    }
    
}