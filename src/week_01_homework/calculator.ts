export class Calculator {
    public Add(x: number, y: number): number {
        return x + y
    }

    public Subtract(x: number, y: number): number {
        return x - y
    }

    public Multiply(x: number, y: number): number {
        return x * y
    }

    public Divide(dividend: number, divisor: number): number {
        if (divisor === 0) {
            throw new Error('Error: Division by zero')
        }

        return dividend / divisor
    }

    public SquareRoot(x: number): number {
        if (x < 0) {
            throw new Error('Error: Cannot calculate square root of a negative number')
        }

        return Math.sqrt(x)
    }

    public Power(base: number, exponent: number): number {
        return Math.pow(base, exponent)
    }
}