export class UnknownError extends Error {

    public constructor(message: string, errorObject?: Error) {
        super(message);
        this.name = this.constructor.name;
    }
    
}