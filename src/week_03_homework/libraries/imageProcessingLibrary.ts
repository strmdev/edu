import { delay } from '../common/delay';

export class ImageProcessingLibrary {

    public async processImage(inputFileWithPath: string): Promise<string> {
        console.log(`We work on image (${inputFileWithPath})...`);
        await delay(1500);

        return 'processed content of image';
    }

}