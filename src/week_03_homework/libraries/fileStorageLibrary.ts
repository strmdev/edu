import { delay } from '../common/delay';

export class FileStorageLibrary {

    public async saveContentIntoFile(processedContentOfImage: string, outputFileWithPath: string): Promise<void> {
        console.log(`Saving image content ${processedContentOfImage} to path ${outputFileWithPath}`);
        await delay(1500);
        console.log('File saved successfully');
    }

}