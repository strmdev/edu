import { FileStorageLibrary } from './libraries/fileStorageLibrary';
import { ImageProcessingLibrary } from './libraries/imageProcessingLibrary';
import { InvalidImageFormatException } from './exceptions/InvalidImageFormatException';
import { ProcessingErrorException } from './exceptions/ProcessingErrorException';
import { UnknownError } from './exceptions/UnknownError';
import { InvalidFilePathException } from './exceptions/InvalidFilePathException';

export class ImageProcessor {

    private static readonly EXPECTED_FILE_FORMAT = '.jpg';

    constructor(private fileStorageLibrary: FileStorageLibrary, private imageProcessingLibrary: ImageProcessingLibrary) { }

    public async processAndSaveImage(inputFileWithPath: string, outputFileWithPath: string): Promise<void> {
        this.validateFilePathNullOrEmpty(inputFileWithPath);
        this.validateFilePathNullOrEmpty(outputFileWithPath);

        this.validateImageFileFormat(inputFileWithPath);
        this.validateImageFileFormat(outputFileWithPath);

        const processedContentOfImage = await this.processImage(inputFileWithPath);
        await this.saveProcessedImageContentIntoFile(processedContentOfImage, outputFileWithPath);
    }

    private validateFilePathNullOrEmpty(fileWithPath: string): void {
        const isfileWithPathNullOrUndefinedOrEmpty = fileWithPath == null || fileWithPath.trim() === '';

        if(isfileWithPathNullOrUndefinedOrEmpty) {
            const errorMessage = 'File path is null or undefined or empty string';
            throw new InvalidFilePathException(errorMessage);
        }
    }

    private validateImageFileFormat(fileWithPath: string): void {
        if (!fileWithPath.endsWith(ImageProcessor.EXPECTED_FILE_FORMAT)) {
            const errorMessage = `Invalid image format: ${fileWithPath} Only ${ImageProcessor.EXPECTED_FILE_FORMAT} images are supported.`;
            throw new InvalidImageFormatException(errorMessage);
        }
    }

    private async processImage(inputFileWithPath: string): Promise<string> {
        try {
            return await this.imageProcessingLibrary.processImage(inputFileWithPath);
        } catch (error) {
            if (error instanceof ProcessingErrorException || error instanceof InvalidFilePathException) {
                throw error;
            }

            const errorMessage = 'Unknown error happened';
            throw new UnknownError(errorMessage, error as Error);
        }
    }

    private async saveProcessedImageContentIntoFile(processedContentOfImage: string, outputFileWithPath: string): Promise<void> {
        try {
            this.validateImageFileFormat(outputFileWithPath);
            await this.fileStorageLibrary.saveContentIntoFile(processedContentOfImage, outputFileWithPath);
        } catch (error) {
            if (error instanceof InvalidImageFormatException || error instanceof InvalidFilePathException) {
                throw error;
            }

            const errorMessage = 'Unknown error happened';
            throw new UnknownError(errorMessage, error as Error);
        }
    }

}