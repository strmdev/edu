export class UserInputValidator {

    public validateUserInput(input: string): boolean {
        return (input.length >= 5 && input.length <= 20) && (/^[a-zA-Z0-9]+$/.test(input));
    }
    
}