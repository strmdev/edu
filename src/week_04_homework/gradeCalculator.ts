export class GradeCalculator {

    public calculateGrade(score: number): string {
        return score >= 90 ? 'A' : score >= 80 ? 'B' : score >= 70 ? 'C' : 'D';
    }
    
}