import { PremiumCampaignData } from "./premiumCampaignData";

export class PremiumCampaignDataProcessor {

    public processPremiumCampaignData(premiumCampaignData: PremiumCampaignData): string {
        return !premiumCampaignData.isPremiumSubscriber() ? 'No action taken.' : premiumCampaignData.hasSentCoupon() ? this.getActivatedCouponsInfo(premiumCampaignData) : this.compileCoupons(premiumCampaignData);
    }

    private getActivatedCouponsInfo(premiumCampaignData: PremiumCampaignData): string {
        const searchedEmail = premiumCampaignData.getSearchedEmail();
        const usersWithActivatedCoupons = premiumCampaignData.getEmailsOfActiveUsersInCampaign();
       
        return usersWithActivatedCoupons.indexOf(searchedEmail) !== -1 ? `User found: ${searchedEmail}` : `User (${searchedEmail}) has not activated any coupons.`;
    }

    private compileCoupons(premiumCampaignData: PremiumCampaignData): string {
        const countOfCoupon = premiumCampaignData.getCountOfCoupon();
        const couponProcessingMessage = (count: number) => `Create ${count}. coupon...`;
    
        return Array.from({ length: countOfCoupon }, (_, indexOfCoupon) => couponProcessingMessage(indexOfCoupon + 1)).join('');
    }
    
}