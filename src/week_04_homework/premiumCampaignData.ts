export class PremiumCampaignData {

    constructor(
        private isPremiumUser: boolean,
        private emailsOfActiveUsersInCampaign: string[],
        private isSentCoupon: boolean,
        private searchedEmail: string,
        private countOfCoupon: number
    ) { }

    public isPremiumSubscriber(): boolean {
        return this.isPremiumUser === true;
    }

    public getEmailsOfActiveUsersInCampaign(): string[] {
        return this.emailsOfActiveUsersInCampaign;
    }

    public hasSentCoupon(): boolean {
        return this.isSentCoupon === true;
    }

    public getSearchedEmail(): string {
        return this.searchedEmail;
    }

    public getCountOfCoupon(): number {
        return this.countOfCoupon;
    }

}