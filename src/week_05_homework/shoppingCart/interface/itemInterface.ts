export interface ItemInterface {

    setProduct(product: string): void;
    getProduct(): string;
    setPrice(price: number): void;
    getPrice(): number;

}
