import { ItemInterface } from './interface/itemInterface';

class ShoppingCart {

    constructor(private items: ItemInterface[] = []) {}

    public addItem(item: ItemInterface): void {
        this.items.push(item);
    }

    public calculateTotal(): number {
        let total = 0;

        for (const item of this.items) {
            total += item.getPrice();
        }
        
        return total;
    }
    
}