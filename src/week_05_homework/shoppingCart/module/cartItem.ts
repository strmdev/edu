import { ItemInterface } from '../interface/itemInterface';

export class CartItem implements ItemInterface{

    constructor(private product: string, private price: number) {}

    public setProduct(product: string): void {
        this.product = product;
    }

    public getProduct(): string {
        return this.product;
    }

    public setPrice(price: number): void {
        this.price = price;
    }

    public getPrice(): number {
        return this.price;
    }

}