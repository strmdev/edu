import { AbstractDiscountCalculator } from './base/abstractDiscountCalculator';

export class SilverDiscountCalculator extends AbstractDiscountCalculator {

    private static readonly SILVER_DISCOUNT_PERCENTAGE = 10;

    public calculateDiscountPercentage(): number {
        return SilverDiscountCalculator.SILVER_DISCOUNT_PERCENTAGE;
    }

}