import { AbstractDiscountCalculator } from './base/abstractDiscountCalculator';

export class StandardDiscountCalculator extends AbstractDiscountCalculator {

    private static readonly STANDARD_DISCOUNT_PERCENTAGE = 5;

    public calculateDiscountPercentage(): number {
        return StandardDiscountCalculator.STANDARD_DISCOUNT_PERCENTAGE;
    }

}