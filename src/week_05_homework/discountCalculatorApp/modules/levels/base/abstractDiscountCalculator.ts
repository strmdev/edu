export abstract class AbstractDiscountCalculator {

    public abstract calculateDiscountPercentage(): number;

}