import { AbstractDiscountCalculator } from './base/abstractDiscountCalculator';

export class GoldDiscountCalculator extends AbstractDiscountCalculator {

    private static readonly GOLD_DISCOUNT_PERCENTAGE = 15;
    private static readonly EXTRA_DISCOUNT_PERCENTAGE = 5;

    public calculateDiscountPercentage(): number {
        return GoldDiscountCalculator.GOLD_DISCOUNT_PERCENTAGE + GoldDiscountCalculator.EXTRA_DISCOUNT_PERCENTAGE;
    }

}