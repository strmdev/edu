import { DiscountCalculatorFactory } from './factory/discountCalculatorFactory';

export class DiscountCalculator {

    private static readonly DEFAULT_DISCOUNT_PERCENTAGE = 0;

    public calculateDiscountPercentage(level: string): number {
        const discountCalculator = DiscountCalculatorFactory.createDiscountCalculator(level);
        return discountCalculator ? discountCalculator.calculateDiscountPercentage() : DiscountCalculator.DEFAULT_DISCOUNT_PERCENTAGE;
    }

}