import { AbstractDiscountCalculator } from "../modules/levels/base/abstractDiscountCalculator";
import { GoldDiscountCalculator } from "../modules/levels/goldDiscountCalculator";
import { SilverDiscountCalculator } from "../modules/levels/silverDiscountCalculator";
import { StandardDiscountCalculator } from "../modules/levels/standardDiscountCalculator";

export class DiscountCalculatorFactory {

    private static discountCalculatorClasses: { [level: string]: new () => AbstractDiscountCalculator } = {
        'standard': StandardDiscountCalculator,
        'silver': SilverDiscountCalculator,
        'gold': GoldDiscountCalculator,
    };

    public static createDiscountCalculator(level: string): AbstractDiscountCalculator | undefined {
        const discountCalculatorClass = this.discountCalculatorClasses[level];
        return discountCalculatorClass ? new discountCalculatorClass() : undefined;
    }

}