import { SchoolClass } from "./modules/schoolClass";

export class School {

    constructor(private classes: SchoolClass[] = []) {}

    public addClass(schoolClass: SchoolClass): void {
        this.classes.push(schoolClass);
    }

    public getStudentCount(): number {
        let totalStudents = 0;

        for (const schoolClass of this.classes) {
            totalStudents += schoolClass.getStudentCount();
        }

        return totalStudents;
    }

}