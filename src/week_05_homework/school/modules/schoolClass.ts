import { Student } from './student';

export class SchoolClass {

    constructor(private students: Student[] = []) {}

    public addStudent(student: Student): void {
        this.students.push(student);
    }

    public getStudentCount(): number {
        return this.students.length;
    }

    public getStudents(): Student[] {
        return this.students;
    }
    
}