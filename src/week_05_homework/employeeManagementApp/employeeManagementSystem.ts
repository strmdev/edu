import { Employee } from "./modules/employee";
import { EmployeeReportGeneratorInterface } from "./interfaces/employeeReportGeneratorInterface";
import { PayrollCalculatorInterface } from "./interfaces/payrollCalculatorInterface";
import { PromoteEmployeeInterface } from "./interfaces/promoteEmployeeInterface";

export class EmployeeManagementSystem {
    
    constructor(
        private employees: Employee[] = [],
        private payrollCalculatorInterface: PayrollCalculatorInterface,
        private employeeReportGeneratorInterface: EmployeeReportGeneratorInterface,
        private promoteEmployeeInterface: PromoteEmployeeInterface
    ) {}

    public addEmployee(employee: Employee): void {
        this.employees.push(employee);
    }

    public calculatePayroll(): number {
        return this.payrollCalculatorInterface.calculatePayroll(this.employees);
    }

    public generateReports(): string {
        return this.employeeReportGeneratorInterface.generateReports(this.employees);
    }

    public promoteEmployee(employee: Employee): void {
        this.promoteEmployeeInterface.promoteEmployee(employee);
    }
}