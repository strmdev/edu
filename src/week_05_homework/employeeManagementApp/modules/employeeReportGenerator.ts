import { Employee } from "./employee";
import { EmployeeReportGeneratorInterface } from "../interfaces/employeeReportGeneratorInterface";

export class EmployeeReportGenerator implements EmployeeReportGeneratorInterface{

    public generateReports(employees: Employee[]): string {
        return "Employee reports: ...";
    }

}