import { Employee } from "./employee";
import { PromoteEmployeeInterface } from "../interfaces/promoteEmployeeInterface";

export class PromoteEmployee implements PromoteEmployeeInterface{

    public promoteEmployee(employee: Employee): void {}

}