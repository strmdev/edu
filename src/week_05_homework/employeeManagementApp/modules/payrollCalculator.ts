import { Employee } from "./employee";
import { PayrollCalculatorInterface } from "../interfaces/payrollCalculatorInterface";

export class PayrollCalculator implements PayrollCalculatorInterface{

    public calculatePayroll(employees: Employee[]): number {
        let totalPayroll = 0;

        for (const employee of employees) {
            totalPayroll += employee.calculateSalary();
        }
        
        return totalPayroll;
    }

}