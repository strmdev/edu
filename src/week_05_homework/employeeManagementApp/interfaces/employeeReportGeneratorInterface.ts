import { Employee } from "../modules/employee";

export interface EmployeeReportGeneratorInterface {

    generateReports(employees: Employee[]): string;

}