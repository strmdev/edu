import { Employee } from "../modules/employee";

export interface PayrollCalculatorInterface {

    calculatePayroll(employees: Employee[]): number;

}