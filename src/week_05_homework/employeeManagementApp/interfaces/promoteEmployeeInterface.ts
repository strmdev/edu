import { Employee } from "../modules/employee";

export interface PromoteEmployeeInterface {

    promoteEmployee(employee: Employee): void;

}