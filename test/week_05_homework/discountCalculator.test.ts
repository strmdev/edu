import { DiscountCalculator } from '../../src/week_05_homework/discountCalculatorApp/discountCalculator';

describe('Discount calculator unit tests', () => {

    let discountCalculator: DiscountCalculator;

    beforeEach(() => {
        discountCalculator = new DiscountCalculator();
    })

    describe('Happy paths', () => {

        it.each`
            level  | discountPercentage
            ${'standard'} | ${5}
            ${'silver'} | ${10}
            ${'gold'} | ${20}
            ${'other'} | ${0}
        `('Given I have a "$level" level, When I calculate the discount percentage, Then the expected result is "$discountPercentage" discount percentage', ({level, discountPercentage}) => {
            // Arrange
            const levelOfMember = level;
            const expectedDiscountPercentage = discountPercentage;

            // Act
            const result = discountCalculator.calculateDiscountPercentage(levelOfMember);

            // Assert
            expect(result).toBe(expectedDiscountPercentage);
        })

    })

})