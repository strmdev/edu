import { mock, mockReset } from 'jest-mock-extended';
import { Employee } from '../../src/week_05_homework/employeeManagementApp/modules/employee';
import { EmployeeManagementSystem } from '../../src/week_05_homework/employeeManagementApp/employeeManagementSystem';
import { PayrollCalculatorInterface } from '../../src/week_05_homework/employeeManagementApp/interfaces/payrollCalculatorInterface';
import { EmployeeReportGeneratorInterface } from '../../src/week_05_homework/employeeManagementApp/interfaces/employeeReportGeneratorInterface';
import { PromoteEmployeeInterface } from '../../src/week_05_homework/employeeManagementApp/interfaces/promoteEmployeeInterface';

class EmployeeTestDataBuilder {

    public static createTestEmployeeDataInstance(name: string = 'Kis Béla', salary: number = 100_000): Employee {
        return new Employee(name, salary);
    }

    public static createTestEmployeesDataInstance(): Employee[] {
        const employees = [
            this.createTestEmployeeDataInstance(),
            this.createTestEmployeeDataInstance('Nagy Béla', 200_000),
            this.createTestEmployeeDataInstance('Létra Elemér', 300_000)
        ];

        return employees;
    }

} 

const mockedPayrollCalculatorInterface = mock<PayrollCalculatorInterface>();
const mockedEmployeeReportGeneratorInterface = mock<EmployeeReportGeneratorInterface>();
const mockedPromoteEmployeeInterface = mock<PromoteEmployeeInterface>();

describe('EmployeeManagementSystem unit tests', () => {

    let employeeManagementSystem: EmployeeManagementSystem;
  
    beforeEach(() => {
        mockReset(mockedPayrollCalculatorInterface);
        mockReset(mockedEmployeeReportGeneratorInterface);
        mockReset(mockedPromoteEmployeeInterface);

        employeeManagementSystem = new EmployeeManagementSystem(
            [],
            mockedPayrollCalculatorInterface,
            mockedEmployeeReportGeneratorInterface,
            mockedPromoteEmployeeInterface
        );
    });

    describe('Happy paths', () => {

        it('Should add an employee', () => {
            // Arrange
            const employeesPrivateProperty = employeeManagementSystem['employees'];
            const expectedEmployee = EmployeeTestDataBuilder.createTestEmployeeDataInstance();
    
            // Act
            employeeManagementSystem.addEmployee(expectedEmployee);
    
            // Assert
            expect(employeesPrivateProperty).toContain(expectedEmployee);
            expect(employeesPrivateProperty).toHaveLength(1);
        });
    
        it('Should calculate payroll', () => {
            // Arrange
            employeeManagementSystem['employees'] = EmployeeTestDataBuilder.createTestEmployeesDataInstance();
            const expectedTotalPayroll = 600_000;
            mockedPayrollCalculatorInterface.calculatePayroll.mockReturnValue(expectedTotalPayroll);
    
            // Act
            const result = employeeManagementSystem.calculatePayroll();
    
            // Assert
            expect(result).toBe(expectedTotalPayroll);
            expect(mockedPayrollCalculatorInterface.calculatePayroll).toHaveBeenCalledTimes(1);
            expect(mockedPayrollCalculatorInterface.calculatePayroll).toHaveBeenCalledWith(employeeManagementSystem['employees']);
        });
    
        it('Should generate reports', () => {
            // Arrange
            employeeManagementSystem['employees'] = EmployeeTestDataBuilder.createTestEmployeesDataInstance();
            const expectedReports = 'Generated employee reports...';
            mockedEmployeeReportGeneratorInterface.generateReports.mockReturnValue(expectedReports);
            
            // Act
            const result = employeeManagementSystem.generateReports();
    
            // Assert
            expect(result).toBe(expectedReports);
            expect(mockedEmployeeReportGeneratorInterface.generateReports).toHaveBeenCalledTimes(1);
            expect(mockedEmployeeReportGeneratorInterface.generateReports).toHaveBeenCalledWith(employeeManagementSystem['employees']);
        });
    
        it('Should promote an employee', () => {
            // Arrange
            const employee = EmployeeTestDataBuilder.createTestEmployeeDataInstance();
            
            // Act
            employeeManagementSystem.promoteEmployee(employee);
            
            // Assert
            expect(mockedPromoteEmployeeInterface.promoteEmployee).toHaveBeenCalledTimes(1);
            expect(mockedPromoteEmployeeInterface.promoteEmployee).toHaveBeenCalledWith(employee);
         });
    
        it('Should create an employee with the given name and salary', () => {
            // Arrange
            const name = 'Alma Szilvia'
            const salary = 500_000;
        
            // Act
            const employee = new Employee(name, salary);
        
            // Assert
            expect(employee).toBeDefined();
        });

        it('Should return the salary of the created employee', () => {
            // Arrange
            const employee = EmployeeTestDataBuilder.createTestEmployeeDataInstance();
            const expectedSalary = 100_000;
        
            // Act
            const result = employee.calculateSalary();
        
            // Assert
            expect(result).toBe(expectedSalary);
        });

    })

});
