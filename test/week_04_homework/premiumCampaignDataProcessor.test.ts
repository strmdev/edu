import { PremiumCampaignData } from '../../src/week_04_homework/premiumCampaignData';
import { PremiumCampaignDataProcessor } from '../../src/week_04_homework/premiumCampaignDataProcessor';

class PremiumCampaignTestDataBuilder {

    public static createTestPremiumCampaignDataInstance(
        isPremiumUser: boolean,
        emailsOfActiveUsersInCampaign: string[],
        isSentCoupon: boolean,
        searchedEmail: string,
        countOfCoupon: number
    ): PremiumCampaignData {
        return new PremiumCampaignData(isPremiumUser, emailsOfActiveUsersInCampaign, isSentCoupon, searchedEmail, countOfCoupon);
    }

}

describe('Premium campaign data processor unit tests', () => {
    let premiumCampaignDataProcessor: PremiumCampaignDataProcessor;

    beforeEach(() => {
        premiumCampaignDataProcessor = new PremiumCampaignDataProcessor();
    })

    describe('Happy paths', () => {

        it.each`
            preCondition | isPremiumUser | emailsOfActiveUsersInCampaign | isSentCoupon | searchedEmail | countOfCoupon | resultMessage
            ${'a premium user who activated the coupon'} | ${true} | ${['strmdev@example.com', 'john@example.com']} | ${true} | ${'strmdev@example.com'} | ${2} | ${'User found: strmdev@example.com'}
            ${'a premium user who has not activated the coupon'} | ${true} | ${['john@example.com']} | ${true} | ${'strmdev@example.com'} | ${2} | ${'User (strmdev@example.com) has not activated any coupons.'}
            ${'a premium user who has not received a coupon yet'} | ${true} | ${['john@example.com']} | ${false} | ${'strmdev@example.com'} | ${2} | ${'Create 1. coupon...Create 2. coupon...'}
            ${'not a premium user'} | ${false} | ${['john@example.com']} | ${false} | ${'strmdev@example.com'} | ${0} | ${'No action taken.'}
        `('I have $preCondition, When I process campaign data, Then the result: $resultMessage', ({isPremiumUser, emailsOfActiveUsersInCampaign, isSentCoupon, searchedEmail, countOfCoupon, resultMessage}) => {
            // Arrange
            const premiumCampaignData = PremiumCampaignTestDataBuilder.createTestPremiumCampaignDataInstance(
                isPremiumUser,
                emailsOfActiveUsersInCampaign,
                isSentCoupon,
                searchedEmail,
                countOfCoupon
            );
            const expectedResult = resultMessage;

            // Act
            const result = premiumCampaignDataProcessor.processPremiumCampaignData(premiumCampaignData);

            // Assert
            expect(result).toBe(expectedResult);
        })


    })
})