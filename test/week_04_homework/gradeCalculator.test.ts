import { GradeCalculator } from '../../src/week_04_homework/gradeCalculator';

describe('Grade calculator unit tests', () => {

    let gradeCalculator: GradeCalculator;

    beforeEach(() => {
        gradeCalculator = new GradeCalculator();
    })

    describe('Happy paths', () => {

        it.each`
            score  | grade
            ${100} | ${'A'}
            ${95}  | ${'A'}
            ${90}  | ${'A'}
            ${85}  | ${'B'}
            ${80}  | ${'B'}
            ${75}  | ${'C'}
            ${70}  | ${'C'}
            ${69}  | ${'D'}
            ${0}   | ${'D'}
        `('Given I have a "$score" score, When I calculate the grade, Then the expected result is "$grade" grade', ({score, grade}) => {
            // Arrange
            const inputScore = score;
            const expectedGrade = grade;

            // Act
            const result = gradeCalculator.calculateGrade(inputScore);

            // Assert
            expect(result).toBe(expectedGrade);
        })

    })

})