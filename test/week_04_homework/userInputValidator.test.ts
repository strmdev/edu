import { UserInputValidator } from '../../src/week_04_homework/userInputValidator';

describe('User input validator unit tests', () => {
    let userInputValidator: UserInputValidator;

    beforeEach(() => {
        userInputValidator = new UserInputValidator();
    })

    describe('Happy paths', () => {

        it.each`
            preCondition | input
            ${'input with exactly 5 characters, consisting of letters and numbers'} ${'strm0'}
            ${'input with exactly 20 characters, consisting of letters and numbers'} ${'strmDev0123456789012'}
            ${'input with more than 5 characters but less than 20 characters, consisting of letters and numbers'} ${'strmDev0'}
            ${'input with exactly 5 characters, consisting only of letters'} ${'strmD'}
            ${'input with exactly 20 characters, consisting only of letters.'} ${'strmDevstrmDevstrmDe'}
            ${'input with more than 5 characters but less than 20 characters, consisting only of letters'} ${'strmDev'}
            ${'input with exactly 5 characters, consisting only of numbers'} ${'01234'}
            ${'input with exactly 20 characters, consisting only of numbers.'} ${'01234567890123456789'}
            ${'input with more than 5 characters but less than 20 characters, consisting only of numbers'} ${'012345'}
        `('Given I have an $preCondition, When I validate user input, Then the expected result is valid', ({input}) => {
            // Arrange
            const inputValue = input;
            const expectedResult = true;

            // Act
            const result = userInputValidator.validateUserInput(inputValue);

            // Assert
            expect(result).toBe(expectedResult);
        })

    })

    describe('Error paths', () => {

        it.each`
            preCondition | input
            ${'empty input'} ${''}
            ${'input containing special characters'} ${'$%strmDev$%'}
            ${'input with less than 5 characters'} ${'strm'}
            ${'input with more than 20 characters'} ${'strmDev01234567890123'}
            ${'input with leading and trailing whitespace'} ${'    strmDev012    '}
        `('Given I have an $preCondition, When I validate user input, Then the expected result is invalid', ({input}) => {
            // Arrange
            const inputValue = input;
            const expectedResult = false;

            // Act
            const result = userInputValidator.validateUserInput(inputValue);

            // Assert
            expect(result).toBe(expectedResult);
        })
        
    })

})