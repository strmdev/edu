import { CurrencyConverter } from '../../src/week_02_homework/currencyConverter';
import { IExchangeRateService } from '../../src/week_02_homework/exchangeRateService';
import { mock, mockReset } from 'jest-mock-extended';
import { ResourceNotFoundError } from '../../src/week_02_homework/exceptions/ResourceNotFoundError';
import { UnknownError } from '../../src/week_02_homework/exceptions/UnknownError';
import { ValidationError } from '../../src/week_02_homework/exceptions/ValidationError';

const mockedExchangeRateService = mock<IExchangeRateService>();

describe('Currency converter unit tests', () => {
    let currencyConverter: CurrencyConverter;

    beforeEach(() => {
        mockReset(mockedExchangeRateService);
        currencyConverter = new CurrencyConverter(mockedExchangeRateService);
    })

    describe('CurrencyConverter.Convert function happy path', () => {
        it('Given I have a valid amount and exchange rate, When I convert the amount from HUF to USD, Then the result is returned.', () => {
            // Arrange
            const exchangeRate = 0.00274259;
            mockedExchangeRateService.getExchangeRate.mockReturnValue(exchangeRate);

            const fromCurrency = 'HUF';
            const toCurrency = 'USD';
            const amount = 1000;
            const expected = 2.74259;

            // Act
            const result = currencyConverter.Convert(amount, fromCurrency, toCurrency);

            // Assert
            expect(result).toBeCloseTo(expected);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledTimes(1);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledWith(fromCurrency, toCurrency);
            expect(mockedExchangeRateService.getExchangeRate).toHaveReturnedWith(exchangeRate);
        })
    })

    describe('CurrencyConverter.Convert function error paths', () => {
        it('Given I have an invalid amount (NaN) and valid exchange rate, When I convert the amount from HUF to USD, Then it should throw an error.', () => {
            // Arrange
            const exchangeRate = 0.00274259;
            mockedExchangeRateService.getExchangeRate.mockReturnValue(exchangeRate);

            const fromCurrency = 'HUF';
            const toCurrency = 'USD';
            const amount = NaN;

            const errorMessage = 'Invalid amount input.';
            const expectedError = new ValidationError(errorMessage);

            // Act and Assert
            expect(() => currencyConverter.Convert(amount, fromCurrency, toCurrency)).toThrow(expectedError);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledTimes(0);
        })

        it('Given I have a valid amount and invalid exchange rate (0), When I convert the amount from HUF to USD, Then it should throw an error.', () => {
            // Arrange
            const exchangeRate = 0;
            mockedExchangeRateService.getExchangeRate.mockReturnValue(exchangeRate);

            const fromCurrency = 'HUF';
            const toCurrency = 'USD';
            const amount = 1000;

            const errorMessage = 'Unable to fetch exchange rate.';
            const expectedError = new ValidationError(errorMessage);
        
            // Act and Assert
            expect(() => currencyConverter.Convert(amount, fromCurrency, toCurrency)).toThrow(expectedError);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledTimes(1);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledWith(fromCurrency, toCurrency);
            expect(mockedExchangeRateService.getExchangeRate).toHaveReturnedWith(exchangeRate);
        })


        it('Given I have a valid amount and invalid exchange rate (NaN), When I convert the amount from HUF to USD, Then it should throw an error.', () => {
            // Arrange
            const exchangeRate = NaN;
            mockedExchangeRateService.getExchangeRate.mockReturnValue(exchangeRate);

            const fromCurrency = 'HUF';
            const toCurrency = 'USD';
            const amount = 1000;

            const errorMessage = 'Invalid exchange rate.';
            const expectedError = new ValidationError(errorMessage);

            // Act and Assert
            expect(() => currencyConverter.Convert(amount, fromCurrency, toCurrency)).toThrow(expectedError);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledTimes(1);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledWith(fromCurrency, toCurrency);
            expect(mockedExchangeRateService.getExchangeRate).toHaveReturnedWith(exchangeRate);
        })

        it('Given I have a valid amount, When I convert the amount from HUF to invalid currency, Then it should throw an error.', () => {
            // Arrange
            const errorMessage = 'The resource you requested could not be found.';
            const expectedError = new ResourceNotFoundError(errorMessage);
            mockedExchangeRateService.getExchangeRate.mockImplementation(() => { throw expectedError });

            const fromCurrency = 'HUF';
            const toCurrency = 'USDDDDDD';
            const amount = 1000;

            // Act and Assert
            expect(() => currencyConverter.Convert(amount, fromCurrency, toCurrency)).toThrow(expectedError);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledTimes(1);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledWith(fromCurrency, toCurrency);
        })

        it('Given I have a valid amount, When I convert the amount from HUF to USD and encounter an unknown error, Then it should throw an error.', () => {
            // Arrange
            const errorMessage = 'Unknown error happened.';
            const expectedError = new UnknownError(errorMessage);
            mockedExchangeRateService.getExchangeRate.mockImplementation(() => { throw expectedError });

            const fromCurrency = 'HUF';
            const toCurrency = 'USD';
            const amount = 1000;

            // Act and Assert
            expect(() => currencyConverter.Convert(amount, fromCurrency, toCurrency)).toThrow(expectedError);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledTimes(1);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledWith(fromCurrency, toCurrency);
        })
    })

    describe('CurrencyConverter.GenerateConversionReport function happy path', () => {
        it('Given I have a valid from/to currency and start/end dates for generating a report, When I generate the report, Then the result is returned.', () => {
            // Arrange
            const exchangeRateFirstReturnValue = 0.00274259;
            const exchangeRateSecondReturnValue = 0.00374259;
            const exchangeRateThirdReturnValue = 0.00474259;
            mockedExchangeRateService.getExchangeRate.mockReturnValueOnce(exchangeRateFirstReturnValue)
                                                     .mockReturnValueOnce(exchangeRateSecondReturnValue)
                                                     .mockReturnValueOnce(exchangeRateThirdReturnValue);

            const fromCurrency = 'HUF';
            const toCurrency = 'USD';
            const startDate = new Date('2023-03-29');
            const endDate = new Date('2023-03-31');

            const numberOfExchangeRate = 3;

            // Act
            const result = currencyConverter.GenerateConversionReport(fromCurrency, toCurrency, startDate, endDate);

            // Assert
            expect(result).toMatchSnapshot();
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledTimes(numberOfExchangeRate);

            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenNthCalledWith(1, fromCurrency, toCurrency);
            expect(mockedExchangeRateService.getExchangeRate).toHaveNthReturnedWith(1, exchangeRateFirstReturnValue);

            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenNthCalledWith(2, fromCurrency, toCurrency);
            expect(mockedExchangeRateService.getExchangeRate).toHaveNthReturnedWith(2, exchangeRateSecondReturnValue);

            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenNthCalledWith(3, fromCurrency, toCurrency);
            expect(mockedExchangeRateService.getExchangeRate).toHaveNthReturnedWith(3, exchangeRateThirdReturnValue);
        })
    })

    describe('CurrencyConverter.GenerateConversionReport function error paths', () => {
        it('Given I have a valid from/to currency and start/end dates, and an invalid exchange rate (NaN) for generating a report, When I generate the report, Then it should throw an error.', () => {
            // Arrange
            const exchangeRateFirstReturnValue = 0.00274259;
            const exchangeRateSecondReturnValue = 0.00374259;
            const exchangeRateThirdReturnValue = NaN;
            mockedExchangeRateService.getExchangeRate.mockReturnValueOnce(exchangeRateFirstReturnValue)
                                                     .mockReturnValueOnce(exchangeRateSecondReturnValue)
                                                     .mockReturnValueOnce(exchangeRateThirdReturnValue);

            const fromCurrency = 'HUF';
            const toCurrency = 'USD';
            const startDate = new Date('2023-03-29');
            const endDate = new Date('2023-03-31');

            const errorMessage = 'Invalid exchange rate.';
            const expectedError = new ValidationError(errorMessage);

            const numberOfExchangeRate = 3;

            // Act and Assert
            expect(() => currencyConverter.GenerateConversionReport(fromCurrency, toCurrency, startDate, endDate)).toThrow(expectedError);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledTimes(numberOfExchangeRate);

            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenNthCalledWith(1, fromCurrency, toCurrency);
            expect(mockedExchangeRateService.getExchangeRate).toHaveNthReturnedWith(1, exchangeRateFirstReturnValue);

            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenNthCalledWith(2, fromCurrency, toCurrency);
            expect(mockedExchangeRateService.getExchangeRate).toHaveNthReturnedWith(2, exchangeRateSecondReturnValue);

            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenNthCalledWith(3, fromCurrency, toCurrency);
            expect(mockedExchangeRateService.getExchangeRate).toHaveNthReturnedWith(3, exchangeRateThirdReturnValue);
        })

        it('Given I have a valid from/to currency and start/end dates, and an invalid exchange rate (0) for generating a report, When I generate the report, Then it should throw an error.', () => {
            // Arrange
            const exchangeRateFirstReturnValue = 0.00274259;
            const exchangeRateSecondReturnValue = 0.00374259;
            const exchangeRateThirdReturnValue = 0;
            mockedExchangeRateService.getExchangeRate.mockReturnValueOnce(exchangeRateFirstReturnValue)
                                                     .mockReturnValueOnce(exchangeRateSecondReturnValue)
                                                     .mockReturnValueOnce(exchangeRateThirdReturnValue);

            const fromCurrency = 'HUF';
            const toCurrency = 'USD';
            const startDate = new Date('2023-03-29');
            const endDate = new Date('2023-03-31');

            const errorMessage = 'Unable to fetch exchange rate.';
            const expectedError = new ValidationError(errorMessage);

            const numberOfExchangeRate = 3;

            // Act and Assert
            expect(() => currencyConverter.GenerateConversionReport(fromCurrency, toCurrency, startDate, endDate)).toThrow(expectedError);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledTimes(numberOfExchangeRate);

            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenNthCalledWith(1, fromCurrency, toCurrency);
            expect(mockedExchangeRateService.getExchangeRate).toHaveNthReturnedWith(1, exchangeRateFirstReturnValue);

            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenNthCalledWith(2, fromCurrency, toCurrency);
            expect(mockedExchangeRateService.getExchangeRate).toHaveNthReturnedWith(2, exchangeRateSecondReturnValue);

            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenNthCalledWith(3, fromCurrency, toCurrency);
            expect(mockedExchangeRateService.getExchangeRate).toHaveNthReturnedWith(3, exchangeRateThirdReturnValue);
        })

        it('Given I have a valid from currency, start/end dates and an invalid to currency for generating a report, When I generate the report, Then it should throw an error.', () => {
            // Arrange
            const errorMessage = 'The resource you requested could not be found.';
            const expectedError = new ResourceNotFoundError(errorMessage);
            mockedExchangeRateService.getExchangeRate.mockImplementation(() => { throw expectedError });

            const fromCurrency = 'HUF';
            const toCurrency = 'USDDD';
            const startDate = new Date('2023-03-29');
            const endDate = new Date('2023-03-31');

            // Act and Assert
            expect(() => currencyConverter.GenerateConversionReport(fromCurrency, toCurrency, startDate, endDate)).toThrow(expectedError);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledTimes(1);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenNthCalledWith(1, fromCurrency, toCurrency);
        })

        it('Given I have a valid from/to currency and start/end dates for generating a report and encounter an unknown error, Then it should throw an error.', () => {
            // Arrange
            const errorMessage = 'Unknown error happened.';
            const expectedError = new UnknownError(errorMessage);
            mockedExchangeRateService.getExchangeRate.mockImplementation(() => { throw expectedError });

            const fromCurrency = 'HUF';
            const toCurrency = 'USD';
            const startDate = new Date('2023-03-29');
            const endDate = new Date('2023-03-31');

            // Act and Assert
            expect(() => currencyConverter.GenerateConversionReport(fromCurrency, toCurrency, startDate, endDate)).toThrow(expectedError);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenCalledTimes(1);
            expect(mockedExchangeRateService.getExchangeRate).toHaveBeenNthCalledWith(1, fromCurrency, toCurrency);
        })
    })
})