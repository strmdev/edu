import { IDatabaseClient } from '../../../src/week_06_final_assessment/abstraction/clients/IDatabaseClient';
import { CourseRepository } from '../../../src/week_06_final_assessment/repository/courseRepository';
import { mock, mockReset } from 'jest-mock-extended';
import { CourseTestDataBuilder } from '../testDataBuilders/courseTestDataBuilder';
import { CourseStatisticsTestDataBuilder } from '../testDataBuilders/courseStatisticsTestDataBuilder';
import { UnknownError } from '../../../src/week_06_final_assessment/exceptions/unknownError';
import { NetworkError } from '../../../src/week_06_final_assessment/exceptions/networkError';
import { StudentTestDataBuilder } from '../testDataBuilders/studentTestDataBuilder';

const mockedIDatabaseClient = mock<IDatabaseClient>();

describe('Course repository unit tests', () => {
    let courseRepository: CourseRepository;

    beforeEach(() => {
        mockReset(mockedIDatabaseClient);
        courseRepository = new CourseRepository(mockedIDatabaseClient);
    })

    describe('Happy paths', () => {

        it('Should add a course', async () => {
            // Arrange
            const course = CourseTestDataBuilder.createTestCourseDataInstance();

            // Act
            await courseRepository.addCourse(course);

            // Assert
            expect(mockedIDatabaseClient.insertCourse).toHaveBeenCalledTimes(1);
            expect(mockedIDatabaseClient.insertCourse).toHaveBeenCalledWith(course);
        })

        it('Should add student to course', async () => {
            // Arrange
            const student = StudentTestDataBuilder.createTestStudentDataInstance();
            const existingCourseName = 'Clean Code'

            // Act
            await courseRepository.addStudentToCourse(student, existingCourseName);

            // Assert
            expect(mockedIDatabaseClient.insertStudentToCourse).toHaveBeenCalledTimes(1);
            expect(mockedIDatabaseClient.insertStudentToCourse).toHaveBeenCalledWith(student, existingCourseName);
        })

        it('Should get course by name', async () => {
            // Arrange
            const existingCourseName = 'Clean Code';

            const course = CourseTestDataBuilder.createTestCourseDataInstance();
            mockedIDatabaseClient.selectCourseByName.mockResolvedValue(course);

            // Act
            const result = await courseRepository.getCourseByName(existingCourseName);

            // Assert
            expect(result).toBe(course);
            expect(mockedIDatabaseClient.selectCourseByName).toHaveBeenCalledTimes(1);
            expect(mockedIDatabaseClient.selectCourseByName).toHaveBeenCalledWith(existingCourseName);
            expect(mockedIDatabaseClient.selectCourseByName(existingCourseName)).resolves.toBe(course);
        })

        it('Should get courses', async () => {
            // Arrange
            const cleanCodeCourse = CourseTestDataBuilder.createTestCourseDataInstance();
            const devOpsCourse = CourseTestDataBuilder.createTestCourseDataInstance('DevOps');

            const expectedCourses = [cleanCodeCourse, devOpsCourse];
            mockedIDatabaseClient.selectAllCourses.mockResolvedValue(expectedCourses);

            // Act
            const result = await courseRepository.getCourses();

            // Assert
            expect(result).toBe(expectedCourses);
            expect(mockedIDatabaseClient.selectAllCourses).toHaveBeenCalledTimes(1);
            expect(mockedIDatabaseClient.selectAllCourses()).resolves.toBe(expectedCourses);
        })


        it('Should get course statisctics', async () => {
            // Arrange
            const cleanCodeCourseStatistics = CourseStatisticsTestDataBuilder.createTestCourseStatisticsDataInstance();
            const existingCourseName = 'Clean Code';
            mockedIDatabaseClient.selectCourseStatisticsByName.mockResolvedValue(cleanCodeCourseStatistics);

            // Act
            const result = await courseRepository.getCourseStatistics(existingCourseName);

            // Assert
            expect(result).toBe(cleanCodeCourseStatistics);
            expect(mockedIDatabaseClient.selectCourseStatisticsByName).toHaveBeenCalledTimes(1);
            expect(mockedIDatabaseClient.selectCourseStatisticsByName(existingCourseName)).resolves.toBe(cleanCodeCourseStatistics);
        })

    })

    describe('Error paths', () => {

        it('Should throw an error when I add course and the internet connection is slow or unstable', async () => {
            // Arrange
            const course = CourseTestDataBuilder.createTestCourseDataInstance();
            const errorMessage = 'Internet connection is slow or unstable';
            const expectedError = new NetworkError(errorMessage);
            mockedIDatabaseClient.insertCourse.mockImplementation(() => { throw expectedError });

            // Act and assert 
            await expect(() => courseRepository.addCourse(course)).rejects.toThrow(expectedError);
            expect(mockedIDatabaseClient.insertCourse).toHaveBeenCalledTimes(1);
            expect(mockedIDatabaseClient.insertCourse).toHaveBeenCalledWith(course);
        })

        it('Should throw an error when I add course and encounter an unknown error', async () => {
            // Arrange
            const course = CourseTestDataBuilder.createTestCourseDataInstance();
            const errorMessage = 'DbClient failed.';
            const expectedError = new UnknownError(errorMessage);
            mockedIDatabaseClient.insertCourse.mockImplementation(() => { throw expectedError });

            // Act and assert 
            await expect(() => courseRepository.addCourse(course)).rejects.toThrow(expectedError);
            expect(mockedIDatabaseClient.insertCourse).toHaveBeenCalledTimes(1);
            expect(mockedIDatabaseClient.insertCourse).toHaveBeenCalledWith(course);
        })

        it('Should throw an error when I add student to course and the internet connection is slow or unstable', async () => {
            // Arrange
            const student = StudentTestDataBuilder.createTestStudentDataInstance();
            const existingCourseName = 'Clean Code'

            const errorMessage = 'Internet connection is slow or unstable';
            const expectedError = new NetworkError(errorMessage);
            mockedIDatabaseClient.insertStudentToCourse.mockImplementation(() => { throw expectedError });

            // Act
            await expect(() => courseRepository.addStudentToCourse(student, existingCourseName)).rejects.toThrow(expectedError);

            // Assert
            expect(mockedIDatabaseClient.insertStudentToCourse).toHaveBeenCalledTimes(1);
            expect(mockedIDatabaseClient.insertStudentToCourse).toHaveBeenCalledWith(student, existingCourseName);
        })

        it('Should throw an error when I add student to course and encounter an unknown error', async () => {
            // Arrange
            const student = StudentTestDataBuilder.createTestStudentDataInstance();
            const existingCourseName = 'Clean Code'

            const errorMessage = 'DbClient failed.';
            const expectedError = new UnknownError(errorMessage);
            mockedIDatabaseClient.insertStudentToCourse.mockImplementation(() => { throw expectedError });

            // Act
            await expect(() => courseRepository.addStudentToCourse(student, existingCourseName)).rejects.toThrow(expectedError);

            // Assert
            expect(mockedIDatabaseClient.insertStudentToCourse).toHaveBeenCalledTimes(1);
            expect(mockedIDatabaseClient.insertStudentToCourse).toHaveBeenCalledWith(student, existingCourseName);
        })

        it('Should throw an error when I get course by name and the internet connection is slow or unstable', async () => {
            // Arrange
            const existingCourseName = 'Clean Code';

            const errorMessage = 'Internet connection is slow or unstable';
            const expectedError = new NetworkError(errorMessage);
            mockedIDatabaseClient.selectCourseByName.mockImplementation(() => { throw expectedError });

            // Act and assert
            await expect(() => courseRepository.getCourseByName(existingCourseName)).rejects.toThrow(expectedError);
            expect(mockedIDatabaseClient.selectCourseByName).toHaveBeenCalledTimes(1);
            expect(mockedIDatabaseClient.selectCourseByName).toHaveBeenCalledWith(existingCourseName);
        })

        it('Should throw an error when I get course by name and ecounter an unknown error', async () => {
            // Arrange
            const existingCourseName = 'Clean Code';

            const errorMessage = 'DbClient failed.';
            const expectedError = new UnknownError(errorMessage);
            mockedIDatabaseClient.selectCourseByName.mockImplementation(() => { throw expectedError });

            // Act and assert
            await expect(() => courseRepository.getCourseByName(existingCourseName)).rejects.toThrow(expectedError);
            expect(mockedIDatabaseClient.selectCourseByName).toHaveBeenCalledTimes(1);
            expect(mockedIDatabaseClient.selectCourseByName).toHaveBeenCalledWith(existingCourseName);
        })

        it('Should throw an error when I get courses and the internet connection is slow or unstable', async () => {
            // Arrange
            const errorMessage = 'Internet connection is slow or unstable';
            const expectedError = new NetworkError(errorMessage);
            mockedIDatabaseClient.selectAllCourses.mockImplementation(() => { throw expectedError });

            // Act
            await expect(() => courseRepository.getCourses()).rejects.toThrow(expectedError);

            // Assert
            expect(mockedIDatabaseClient.selectAllCourses).toHaveBeenCalledTimes(1);
        })

        it('Should throw an error when I get courses and encounter an unknown error', async () => {
            // Arrange
            const errorMessage = 'DbClient failed.';
            const expectedError = new UnknownError(errorMessage);
            mockedIDatabaseClient.selectAllCourses.mockImplementation(() => { throw expectedError });

            // Act
            await expect(() => courseRepository.getCourses()).rejects.toThrow(expectedError);

            // Assert
            expect(mockedIDatabaseClient.selectAllCourses).toHaveBeenCalledTimes(1);
        })

        it('Should throw an error when I get course statistics and the internet connection is slow or unstable', async () => {
            // Arrange
            const existingCourseName = 'Clean Code';

            const errorMessage = 'Internet connection is slow or unstable';
            const expectedError = new NetworkError(errorMessage);
            mockedIDatabaseClient.selectCourseStatisticsByName.mockImplementation(() => { throw expectedError });

            // Act and assert
            await expect(() => courseRepository.getCourseStatistics(existingCourseName)).rejects.toThrow(expectedError);
            expect(mockedIDatabaseClient.selectCourseStatisticsByName).toHaveBeenCalledTimes(1);
        })

        it('Should throw an error when I get course statistics and encounter an unknown error', async () => {
            // Arrange
            const existingCourseName = 'Clean Code';

            const errorMessage = 'DbClient failed.';
            const expectedError = new UnknownError(errorMessage);
            mockedIDatabaseClient.selectCourseStatisticsByName.mockImplementation(() => { throw expectedError });

            // Act and assert
            await expect(() => courseRepository.getCourseStatistics(existingCourseName)).rejects.toThrow(expectedError);
            expect(mockedIDatabaseClient.selectCourseStatisticsByName).toHaveBeenCalledTimes(1);
        })

    })

})