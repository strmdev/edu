import { EmailClient } from '../../../src/week_06_final_assessment/clients/emailClient';

describe('Email client unit tests', () => {
    let emailClient: EmailClient;

    beforeEach(() => {
        const sender = 'sender@gmail.com';
        const receiver = 'receiver@gmail.com';
        const subject = 'Test subject';

        emailClient = new EmailClient(sender, receiver, subject);
    })

    describe('Happy path', () => {

        it('Given I hava a valid sender and receiver, When I send messsage, Then email client sends it', async () => {
            // Arrange
            const inputMessage = 'I send a message from an email client';
            
            // Act and assert
            await emailClient.sendNotification(inputMessage);
        })

    })

})