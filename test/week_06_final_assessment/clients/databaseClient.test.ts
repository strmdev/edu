import { DatabaseClient } from '../../../src/week_06_final_assessment/clients/databaseClient';
import { CourseTestDataBuilder } from '../../week_06_final_assessment/testDataBuilders/courseTestDataBuilder';
import { StudentTestDataBuilder } from '../../week_06_final_assessment/testDataBuilders/studentTestDataBuilder';

describe('Database client unit tests', () => {
    let databaseClient: DatabaseClient;

    beforeEach(() => {
        databaseClient = new DatabaseClient();
    })

    it('Should insert a course', async () => {
        // Arrange
        const course = CourseTestDataBuilder.createTestCourseDataInstance();

        // Act and assert
        await databaseClient.insertCourse(course);
    })

    it('Should insert student to course', async () => {
        // Arrange
        const student = StudentTestDataBuilder.createTestStudentDataInstance();
        const courseName = 'Clean Code';

        // Act and assert
        await databaseClient.insertStudentToCourse(student, courseName);
    })

    it('Should get course by name', async () => {
        // Arrange
        const expectedCourseName = 'Clean Code';
        const expectedStartDate = new Date(2024, 6, 1);
        const expectedLengthInWeeks = 6;
        const expectedCostInHuf = 100_000;

        // Act
        const result = await databaseClient.selectCourseByName(expectedCourseName);

        // Assert
        expect(result?.getCourseName()).toBe(expectedCourseName);
        expect(result?.getStartDate()).toEqual(expectedStartDate);
        expect(result?.getLengthInWeeks()).toEqual(expectedLengthInWeeks);
        expect(result?.getCostInHuf()).toEqual(expectedCostInHuf);
    })

    it('Should get all courses', async () => {
        // Arrange
        const expectedFirstCourseName = 'Clean Code';
        const expectedFirstStartDate = new Date(2024, 6, 1);
        const expectedFirstLengthInWeeks = 6;
        const expectedFirstCostInHuf = 100_000;

        const expectedSecondCourseName = 'Unit testing';
        const expectedSecondStartDate = new Date(2024, 7, 1);
        const expectedSecondLengthInWeeks = 7;
        const expectedSecondCostInHuf = 200_000;

        const expectedStudentCount = 0;

        // Act
        const result = await databaseClient.selectAllCourses();

        // Assert
        const firstCourse = result[0];
        const secondCourse = result[1];

        expect(firstCourse.getCourseName()).toBe(expectedFirstCourseName);
        expect(firstCourse.getStartDate()).toEqual(expectedFirstStartDate);
        expect(firstCourse.getLengthInWeeks()).toEqual(expectedFirstLengthInWeeks);
        expect(firstCourse.getCostInHuf()).toEqual(expectedFirstCostInHuf);
        expect(firstCourse.getStudentCount()).toEqual(expectedStudentCount);
        expect(firstCourse.getStudents()).toEqual([]);

        expect(secondCourse.getCourseName()).toBe(expectedSecondCourseName);
        expect(secondCourse.getStartDate()).toEqual(expectedSecondStartDate);
        expect(secondCourse.getLengthInWeeks()).toEqual(expectedSecondLengthInWeeks);
        expect(secondCourse.getCostInHuf()).toEqual(expectedSecondCostInHuf);
        expect(secondCourse.getStudentCount()).toBe(expectedStudentCount)
        expect(secondCourse.getStudents()).toEqual([]);
    })

    it('Should get course statistics by name', async () => {
        // Arrange
        const expectedCourseName = 'Clean Code';
        const expectedTotalLectures = 60;
        const expectedLecturesCompleted = 30;
        const expectedProgress = 50;
        const expectedGetLastAccessed = new Date(2024, 4, 29);

        // Act
        const result = await databaseClient.selectCourseStatisticsByName(expectedCourseName);

        // Assert
        expect(result.getCourseName()).toBe(expectedCourseName);
        expect(result.getTotalLectures()).toEqual(expectedTotalLectures);
        expect(result.getLecturesCompleted()).toEqual(expectedLecturesCompleted);
        expect(result.getProgress()).toEqual(expectedProgress);
        expect(result.getLastAccessed()).toEqual(expectedGetLastAccessed);
    })

})