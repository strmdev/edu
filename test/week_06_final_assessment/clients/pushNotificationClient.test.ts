import { PushNotificationClient } from '../../../src/week_06_final_assessment/clients/pushNotificationClient';

describe('Push notification client unit tests', () => {
    let pushNotificationClient: PushNotificationClient;

    beforeEach(() => {
        const deviceToken = 'deviceToken007';

        pushNotificationClient = new PushNotificationClient(deviceToken);
    })

    describe('Happy path', () => {

        it('Given I hava a valid device token, When I send messsage, Then push notification client sends it', async () => {
            // Arrange
            const inputMessage = 'I send a push notification message';
            
            // Act and assert
            await pushNotificationClient.sendNotification(inputMessage);
        })

    })

})