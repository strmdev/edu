import { FinancialApiClient } from '../../../src/week_06_final_assessment/clients/financialApiClient';
import { ResourceNotFoundError } from '../../../src/week_06_final_assessment/exceptions/resourceNotFoundError';
import { Course } from '../../../src/week_06_final_assessment/models/course';
import { Student } from '../../../src/week_06_final_assessment/models/student';

describe('Financial API clients unit tests', () => {
    let financialApiClient: FinancialApiClient;

    beforeEach(() => {
        financialApiClient = new FinancialApiClient();
    })

    describe('Happy paths', () => {

        it('Given I have a valid student, When we check the payment status, Then it is result true', async () => {
            // Arrange
            const course = new Course('Clean Code', new Date(2024, 3, 20), 6, 100_000);
            const student = new Student('Kis Béla', 'kis.bela@gmail.com', '+36301234567', [course]);
            const expectedPaymentStatus = true;

            // Act
            const result = await financialApiClient.checkPaymentStatus(student);

            // Assert
            expect(result).toBe(expectedPaymentStatus);
        })

        it('Given I have a valid student, When student pays for the course, Then it is not throw an error', async () => {
            // Arrange
            const course = new Course('Clean Code', new Date(2024, 3, 20), 6, 100_000);
            const student = new Student('Kis Béla', 'kis.bela@gmail.com', '+36301234567', [course]);

            // Act and assert
            const result = await financialApiClient.makePayment(student);
        })

    })

    describe('Error paths', () => {

        it('Payment status return false with invalid student', async () => {
            // Arrange
            const course = new Course('Clean Code', new Date(2024, 3, 20), 6, 100_000);
            const student = new Student('', 'kis.bela@gmail.com', '+36301234567', [course]);
            const expectedPaymentStatus = false;

            // Act
            const result = await financialApiClient.checkPaymentStatus(student);

            // Assert
            expect(result).toBe(expectedPaymentStatus);
        })

        it('Make payment should throw an error with invalid student', async () => {
            // Arrange
            const course = new Course('Clean Code', new Date(2024, 3, 20), 6, 100_000);
            const student = new Student('', 'kis.bela@gmail.com', '+36301234567', [course]);

            const errorMessage = 'Resource not found';
            const expectedError = new ResourceNotFoundError(errorMessage);

            // Act and assert
            await expect(() => financialApiClient.makePayment(student)).rejects.toThrow(expectedError);
        })

    })


})