import { mock, mockReset } from 'jest-mock-extended';
import { CourseService } from '../../../src/week_06_final_assessment/services/courseService';
import { ICourseRepository } from '../../../src/week_06_final_assessment/abstraction/repository/ICourseRepository';
import { IPaymentService } from '../../../src/week_06_final_assessment/abstraction/services/IPaymentService';
import { INotificationService } from '../../../src/week_06_final_assessment/abstraction/services/INotificationService';
import { ValidationError } from '../../../src/week_06_final_assessment/exceptions/validationError';
import { StudentTestDataBuilder } from '../testDataBuilders/studentTestDataBuilder';
import { CourseTestDataBuilder } from '../testDataBuilders/courseTestDataBuilder';
import { CourseStatisticsTestDataBuilder } from '../testDataBuilders/courseStatisticsTestDataBuilder';

const mockedCourseRepository = mock<ICourseRepository>();
const mockedPaymentService = mock<IPaymentService>();
const mockedNotificationService = mock<INotificationService>();

describe('Course service unit tests', () => {

    let courseService: CourseService;

    beforeEach(() => {
        mockReset(mockedCourseRepository);
        mockReset(mockedPaymentService);
        mockReset(mockedNotificationService);
        courseService = new CourseService(mockedCourseRepository, mockedPaymentService, mockedNotificationService);
    })

    describe('Happy paths', () => {

        it('Should add a course', async () => {
            // Arrange
            const expectedStudent = StudentTestDataBuilder.createTestStudentDataInstance();
            const cleanCodeCourse = CourseTestDataBuilder.createTestCourseDataInstance();
            cleanCodeCourse.addStudent(expectedStudent);

            const expectedCourseName = 'Clean Code';
            const expectedStartDate = new Date(2024, 3, 31);
            const expectedLengthInWeeks = 6;
            const expectedCostInHuf = 100_000;
            const expectedStudentCount = 1;

            // Act
            await courseService.addCourse(cleanCodeCourse);

            // Assert
            expect(mockedCourseRepository.addCourse).toHaveBeenCalledTimes(1);
            expect(mockedCourseRepository.addCourse).toHaveBeenCalledWith(cleanCodeCourse);

            expect(cleanCodeCourse.getCourseName()).toBe(expectedCourseName);
            expect(cleanCodeCourse.getStartDate()).toEqual(expectedStartDate);
            expect(cleanCodeCourse.getLengthInWeeks()).toBe(expectedLengthInWeeks);
            expect(cleanCodeCourse.getCostInHuf()).toEqual(expectedCostInHuf);
            expect(cleanCodeCourse.getStudentCount()).toEqual(expectedStudentCount);
            expect(cleanCodeCourse.getStudents()).toContain(expectedStudent);
        })

        it('Should get courses', async () => {
            // Arrange
            const cleanCodeCourse = CourseTestDataBuilder.createTestCourseDataInstance();
            const devOpsCourse = CourseTestDataBuilder.createTestCourseDataInstance('DevOps');
            const expectedCourses = [cleanCodeCourse, devOpsCourse];

            mockedCourseRepository.getCourses.mockResolvedValue(expectedCourses);

            // Act
            await courseService.getCourses();

            // Assert
            expect(mockedCourseRepository.getCourses).toHaveBeenCalledTimes(1);
            expect(mockedCourseRepository.getCourses()).resolves.toBe(expectedCourses);
        })

        it('Should add a student to course if the course exists and student payed the course', async () => {
            // Arrange
            const student = StudentTestDataBuilder.createTestStudentDataInstance();
            const expectedRegisteredCourse = CourseTestDataBuilder.createTestCourseDataInstance();
            const expectedStudentName = 'Kis Péter';
            const expectedStudentEmailAddress =  'kis.peter@gmail.com';
            const expectedStudentPhoneNumber = '+36301234567';

            const existingCourseName = 'DevOps';
            const existingCourse = CourseTestDataBuilder.createTestCourseDataInstance('DevOps');

            const isOrderPayed = true;
            const expectedNotificationMessage = `${student.getName()} student was added to course.`;
            
            mockedCourseRepository.getCourseByName.mockResolvedValue(existingCourse);
            mockedPaymentService.getIsOrderPayed.mockResolvedValue(isOrderPayed);

            // Act
            await courseService.addStudentToCourse(student, existingCourseName);

            // Assert
            expect(mockedCourseRepository.getCourseByName).toHaveBeenCalledTimes(1);
            expect(mockedCourseRepository.getCourseByName).toHaveBeenCalledWith(existingCourseName);
            expect(mockedCourseRepository.getCourseByName(existingCourseName)).resolves.toBe(existingCourse);

            expect(mockedPaymentService.getIsOrderPayed).toHaveBeenCalledTimes(1);
            expect(mockedPaymentService.getIsOrderPayed).toHaveBeenCalledWith(student);
            expect(mockedPaymentService.getIsOrderPayed(student)).resolves.toBe(isOrderPayed);
            
            expect(mockedCourseRepository.addStudentToCourse).toHaveBeenCalledTimes(1);
            expect(mockedCourseRepository.addStudentToCourse).toHaveBeenCalledWith(student, existingCourseName);

            expect(mockedNotificationService.sendNotifications).toHaveBeenCalledTimes(1);
            expect(mockedNotificationService.sendNotifications).toHaveBeenCalledWith(expectedNotificationMessage);

            expect(student.getName()).toBe(expectedStudentName);
            expect(student.getEmailAddress()).toBe(expectedStudentEmailAddress);
            expect(student.getPhoneNumber()).toBe(expectedStudentPhoneNumber);
            expect(student.getRegisteredCourses()).toContainEqual(expectedRegisteredCourse);
        })

        it('Should get course statistics if the course exists', async () => {
            // Arrange
            const cleanCodeCourse = CourseTestDataBuilder.createTestCourseDataInstance();
            const cleanCodeCourseStatistics = CourseStatisticsTestDataBuilder.createTestCourseStatisticsDataInstance();

            const expectedCourseName = 'Clean Code';
            const expectedLastAccessed = new Date(2024, 3, 31);
            const expectedProgress = 50;
            const expectedLecturesCompleted = 40;
            const expectedTotalLectures = 80;
            
            mockedCourseRepository.getCourseByName.mockResolvedValue(cleanCodeCourse);
            mockedCourseRepository.getCourseStatistics.mockResolvedValue(cleanCodeCourseStatistics);
    
            // Act
            await courseService.getCourseStatistics(expectedCourseName);
    
            // Assert
            expect(mockedCourseRepository.getCourseByName).toHaveBeenCalledTimes(1);
            expect(mockedCourseRepository.getCourseByName).toHaveBeenCalledWith(expectedCourseName);
            expect(mockedCourseRepository.getCourseByName(expectedCourseName)).resolves.toBe(cleanCodeCourse);

            expect(mockedCourseRepository.getCourseStatistics).toHaveBeenCalledTimes(1);
            expect(mockedCourseRepository.getCourseStatistics).toHaveBeenCalledWith(expectedCourseName);
            expect(mockedCourseRepository.getCourseStatistics(expectedCourseName)).resolves.toBe(cleanCodeCourseStatistics);

            expect(cleanCodeCourseStatistics.getCourseName()).toBe(expectedCourseName);
            expect(cleanCodeCourseStatistics.getLastAccessed()).toEqual(expectedLastAccessed);
            expect(cleanCodeCourseStatistics.getProgress()).toBe(expectedProgress)
            expect(cleanCodeCourseStatistics.getLecturesCompleted()).toBe(expectedLecturesCompleted);
            expect(cleanCodeCourseStatistics.getTotalLectures()).toBe(expectedTotalLectures);
        })

    })

    describe('Error paths', () => {

        it('Given I have a student and a nonexistent course, When I add student to course, Then it should throw an error', async () => {
            // Arrange
            const student = StudentTestDataBuilder.createTestStudentDataInstance();
            const nonexistentCourseName = 'Nonexistent Course'

            const errorMessage = 'Course not found';
            const expectedError = new ValidationError(errorMessage);
            
            const getCourseByNameReturnValue = undefined;
            mockedCourseRepository.getCourseByName.mockResolvedValue(getCourseByNameReturnValue);

            // Act
            await expect(() => courseService.addStudentToCourse(student, nonexistentCourseName)).rejects.toThrow(expectedError);

            // Assert
            expect(mockedCourseRepository.getCourseByName).toHaveBeenCalledTimes(1);
            expect(mockedCourseRepository.getCourseByName).toHaveBeenCalledWith(nonexistentCourseName);
            expect(mockedCourseRepository.getCourseByName(nonexistentCourseName)).resolves.toBe(getCourseByNameReturnValue);

            expect(mockedPaymentService.getIsOrderPayed).toHaveBeenCalledTimes(0);
            expect(mockedCourseRepository.addStudentToCourse).toHaveBeenCalledTimes(0);
            expect(mockedNotificationService.sendNotifications).toHaveBeenCalledTimes(0);
        })

        it('Should throw an error when I add a student to course and it is not yet paid by student', async () => {
            // Arrange
            const student = StudentTestDataBuilder.createTestStudentDataInstance();
            
            const courseName = 'Clean Code'
            const course = CourseTestDataBuilder.createTestCourseDataInstance();
            const isOrderPayed = false;
            
            mockedCourseRepository.getCourseByName.mockResolvedValue(course);
            mockedPaymentService.getIsOrderPayed.mockResolvedValue(isOrderPayed);

            const errorMessage = 'Course is not yet paid by Student.';
            const expectedError = new ValidationError(errorMessage);

            // Act
            await expect(() => courseService.addStudentToCourse(student, courseName)).rejects.toThrow(expectedError);

            // Assert
            expect(mockedCourseRepository.getCourseByName).toHaveBeenCalledTimes(1);
            expect(mockedCourseRepository.getCourseByName).toHaveBeenCalledWith(courseName);
            expect(mockedCourseRepository.getCourseByName(courseName)).resolves.toBe(course);

            expect(mockedPaymentService.getIsOrderPayed).toHaveBeenCalledTimes(1);
            expect(mockedPaymentService.getIsOrderPayed).toHaveBeenCalledWith(student);
            expect(mockedPaymentService.getIsOrderPayed(student)).resolves.toBe(isOrderPayed);

            expect(mockedCourseRepository.addStudentToCourse).toHaveBeenCalledTimes(0);
            expect(mockedNotificationService.sendNotifications).toHaveBeenCalledTimes(0);
        })

        it('Should throw an error when I query course statistics for a nonexistent course', async () => {
            // Arrange
            const nonexistentCourseName = 'Nonexistent Course'

            const errorMessage = 'Course not found';
            const expectedError = new ValidationError(errorMessage);
            
            const getCourseByNameReturnValue = undefined;
            mockedCourseRepository.getCourseByName.mockResolvedValue(getCourseByNameReturnValue);
    
            // Act
            await expect(courseService.getCourseStatistics(nonexistentCourseName)).rejects.toThrow(expectedError);
    
            // Assert
            expect(mockedCourseRepository.getCourseByName).toHaveBeenCalledTimes(1);
            expect(mockedCourseRepository.getCourseByName).toHaveBeenCalledWith(nonexistentCourseName);
            expect(mockedCourseRepository.getCourseByName(nonexistentCourseName)).resolves.toBe(getCourseByNameReturnValue);

            expect(mockedCourseRepository.getCourseStatistics).toHaveBeenCalledTimes(0);
        })

    })

})