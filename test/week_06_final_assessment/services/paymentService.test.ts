import { mock, mockReset } from 'jest-mock-extended';
import { IFinancialApiClient } from '../../../src/week_06_final_assessment/abstraction/clients/IFinancialApiClient';
import { PaymentService } from '../../../src/week_06_final_assessment/services/paymentService';
import { NetworkError } from '../../../src/week_06_final_assessment/exceptions/networkError';
import { StudentTestDataBuilder } from '../testDataBuilders/studentTestDataBuilder';
import { ResourceNotFoundError } from '../../../src/week_06_final_assessment/exceptions/resourceNotFoundError';
import { UnknownError } from '../../../src/week_06_final_assessment/exceptions/unknownError';

const RESOURCE_NOT_FOUND_ERROR_MESSAGE = 'Resource not found error.';
const NETWORK_ERROR_MESSAGE = 'Internet connection is slow or unstable.';
const UNKNOWN_ERROR_MESSAGE = 'Unknown error happened.';

const mockedFinancialApiClient = mock<IFinancialApiClient>();

describe('Payment service unit tests', () => {
    let paymentService: PaymentService;

    beforeEach(() => {
        mockReset(mockedFinancialApiClient);
        paymentService = new PaymentService(mockedFinancialApiClient);
    })

    describe('Happy paths', () => {

        it('Given I have a student who paid a course, When I check the payment status, Then its value is successfully', async () => {
            // Arrange
            const student = StudentTestDataBuilder.createTestStudentDataInstance();
            const isSucccessPaymentStatus = true;
            mockedFinancialApiClient.checkPaymentStatus.mockResolvedValue(isSucccessPaymentStatus);
    
            // Act
            const result = await paymentService.getIsOrderPayed(student);
    
            // Assert
            expect(result).toBe(isSucccessPaymentStatus);
            expect(mockedFinancialApiClient.checkPaymentStatus).toHaveBeenCalledTimes(1);
            expect(mockedFinancialApiClient.checkPaymentStatus).toHaveBeenCalledWith(student);
            expect(mockedFinancialApiClient.checkPaymentStatus(student)).resolves.toBe(isSucccessPaymentStatus);
        })

        it('Given I have a student who is paying for a course, When student starts transaction, Then transaction is completed successfully', async () => {
            // Arrange
            const student = StudentTestDataBuilder.createTestStudentDataInstance();
    
            // Act
            await paymentService.makePaymentForCourse(student);
    
            // Assert
            expect(mockedFinancialApiClient.makePayment).toHaveBeenCalledTimes(1);
            expect(mockedFinancialApiClient.makePayment).toHaveBeenCalledWith(student);
        })

    })

    describe('Error paths', () => {

        it('Should throw an error when I check payment status and internet connection is slow or unstable', async () => {
            // Arrange
            const student = StudentTestDataBuilder.createTestStudentDataInstance();
            
            const errorMessage = NETWORK_ERROR_MESSAGE;
            const expectedError = new NetworkError(errorMessage);
            mockedFinancialApiClient.checkPaymentStatus.mockImplementation(() => { throw expectedError });

            // Act and assert
            await expect(() => paymentService.getIsOrderPayed(student)).rejects.toThrow(expectedError);
            expect(mockedFinancialApiClient.checkPaymentStatus).toHaveBeenCalledTimes(1);
            expect(mockedFinancialApiClient.checkPaymentStatus).toHaveBeenCalledWith(student);
        })

        it('Should throw an error when I check payment status and resource not found', async () => {
            // Arrange
            const student = StudentTestDataBuilder.createTestStudentDataInstance();
            
            const errorMessage = RESOURCE_NOT_FOUND_ERROR_MESSAGE;
            const expectedError = new ResourceNotFoundError(errorMessage);
            mockedFinancialApiClient.checkPaymentStatus.mockImplementation(() => { throw expectedError });

            // Act and assert
            await expect(() => paymentService.getIsOrderPayed(student)).rejects.toThrow(expectedError);
            expect(mockedFinancialApiClient.checkPaymentStatus).toHaveBeenCalledTimes(1);
            expect(mockedFinancialApiClient.checkPaymentStatus).toHaveBeenCalledWith(student);
        })

        it('Should throw an error when I check payment status and encounter an unknown error', async () => {
            // Arrange
            const student = StudentTestDataBuilder.createTestStudentDataInstance();
            
            const errorMessage = UNKNOWN_ERROR_MESSAGE;
            const expectedError = new UnknownError(errorMessage);
            mockedFinancialApiClient.checkPaymentStatus.mockImplementation(() => { throw expectedError });

            // Act and assert
            await expect(() => paymentService.getIsOrderPayed(student)).rejects.toThrow(expectedError);
            expect(mockedFinancialApiClient.checkPaymentStatus).toHaveBeenCalledTimes(1);
            expect(mockedFinancialApiClient.checkPaymentStatus).toHaveBeenCalledWith(student);
        })

        it('Should throw an error when student paying for a course and internet connection is slow or unstable', async () => {
            // Arrange
            const student = StudentTestDataBuilder.createTestStudentDataInstance();
            
            const errorMessage = NETWORK_ERROR_MESSAGE;
            const expectedError = new NetworkError(errorMessage);
            mockedFinancialApiClient.makePayment.mockImplementation(() => { throw expectedError });

            // Act and assert
            await expect(() => paymentService.makePaymentForCourse(student)).rejects.toThrow(expectedError);
            expect(mockedFinancialApiClient.makePayment).toHaveBeenCalledTimes(1);
            expect(mockedFinancialApiClient.makePayment).toHaveBeenCalledWith(student);
        })

        it('Should throw an error when student paying for a course and resource not found', async () => {
            // Arrange
            const student = StudentTestDataBuilder.createTestStudentDataInstance();
            
            const errorMessage = RESOURCE_NOT_FOUND_ERROR_MESSAGE;
            const expectedError = new ResourceNotFoundError(errorMessage);
            mockedFinancialApiClient.makePayment.mockImplementation(() => { throw expectedError });

            // Act and assert
            await expect(() => paymentService.makePaymentForCourse(student)).rejects.toThrow(expectedError);
            expect(mockedFinancialApiClient.makePayment).toHaveBeenCalledTimes(1);
            expect(mockedFinancialApiClient.makePayment).toHaveBeenCalledWith(student);
        })

        it('Should throw an error when student paying for a course and encounter an unknown error', async () => {
            // Arrange
            const student = StudentTestDataBuilder.createTestStudentDataInstance();
            
            const errorMessage = UNKNOWN_ERROR_MESSAGE;
            const expectedError = new UnknownError(errorMessage);
            mockedFinancialApiClient.makePayment.mockImplementation(() => { throw expectedError });

            // Act and assert
            await expect(() => paymentService.makePaymentForCourse(student)).rejects.toThrow(expectedError);
            expect(mockedFinancialApiClient.makePayment).toHaveBeenCalledTimes(1);
            expect(mockedFinancialApiClient.makePayment).toHaveBeenCalledWith(student);
        })

    })

})