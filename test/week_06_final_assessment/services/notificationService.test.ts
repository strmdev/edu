import { mock, mockReset } from 'jest-mock-extended';
import { NotificationService } from '../../../src/week_06_final_assessment/services/notificationService';
import { IMessageClient } from '../../../src/week_06_final_assessment/abstraction/clients/IMessageClient';

const mockedFirstIMessageClient = mock<IMessageClient>();
const mockedSecondIMessageClient = mock<IMessageClient>();

describe('Notification service unit test', () => {
    let notificationService: NotificationService;

    mockReset(mockedFirstIMessageClient);
    mockReset(mockedSecondIMessageClient);

    const mockedIMessageClients = [mockedFirstIMessageClient, mockedSecondIMessageClient];
    notificationService = new NotificationService(mockedIMessageClients);

    it('Given I have two notification service, When I send message, Then it send the message across the differents notification services', async () => {
        // Arrange
        const inputMessage = 'Test message';

        // Act
        await notificationService.sendNotifications(inputMessage);

        // Assert
        expect(mockedFirstIMessageClient.sendNotification).toHaveBeenCalledTimes(1);
        expect(mockedFirstIMessageClient.sendNotification).toHaveBeenCalledWith(inputMessage);
        
        expect(mockedSecondIMessageClient.sendNotification).toHaveBeenCalledTimes(1);
        expect(mockedSecondIMessageClient.sendNotification).toHaveBeenCalledWith(inputMessage);
    })

})