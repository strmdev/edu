import { Course } from '../../../src/week_06_final_assessment/models/course';

export class CourseTestDataBuilder {

    public static createTestCourseDataInstance(
        courseName: string = 'Clean Code',
        startDate: Date = new Date(2024, 3, 31),
        lengthInWeeks: number = 6,
        costInHuf: number = 100_000
    ): Course {
        return new Course(courseName, startDate, lengthInWeeks, costInHuf);
    }

}