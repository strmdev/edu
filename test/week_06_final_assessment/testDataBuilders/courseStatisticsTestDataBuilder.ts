import { CourseStatistics } from '../../../src/week_06_final_assessment/models/courseStatistics';

export class CourseStatisticsTestDataBuilder {

    public static createTestCourseStatisticsDataInstance(
        courseName: string = 'Clean Code',
        totalLectures: number = 80,
        lecturesCompleted: number = 40,
        progress: number = 50,
        lastAccessed: Date = new Date(2024, 3, 31)
    ): CourseStatistics {
        return new CourseStatistics(courseName, totalLectures, lecturesCompleted, progress, lastAccessed);
    }

}