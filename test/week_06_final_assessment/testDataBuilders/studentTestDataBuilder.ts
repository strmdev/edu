import { Student } from '../../../src/week_06_final_assessment/models/student';
import { CourseTestDataBuilder } from './courseTestDataBuilder';

export class StudentTestDataBuilder {

    public static createTestStudentDataInstance(
        name: string = 'Kis Péter',
        emailAddress: string = 'kis.peter@gmail.com',
        phoneNumber: string = '+36301234567'
    ): Student {
        const registeredCourse = CourseTestDataBuilder.createTestCourseDataInstance();
        return new Student(name, emailAddress, phoneNumber, [registeredCourse]);
    }

}