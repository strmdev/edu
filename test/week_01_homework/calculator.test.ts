import { Calculator } from '../../src/week_01_homework/calculator'

describe('Calculator tests', () => {
    let calculator: Calculator

    beforeEach(() => {
        calculator = new Calculator()
    })

    describe('Add tests', () => {
        test('Given I have 2 positive numbers, When I add them together, Then the sum of the two numbers is returned', () => {
            // Arrange
            const x = 5
            const y = 3
            const expected = 8

            // Act
            const result = calculator.Add(x, y)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 2 negative numbers, When I add them together, Then the sum of the two numbers is returned', () => {
            // Arrange
            const x = -5
            const y = -3
            const expected = -8

            // Act
            const result = calculator.Add(x, y)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 2 zero numbers, When I add them together, Then the sum of the two numbers is returned', () => {
            // Arrange
            const x = 0
            const y = 0
            const expected = 0

            // Act
            const result = calculator.Add(x, y)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 2 floating-point numbers, When I add them together, Then the sum of the two numbers is returned', () => {
            // Arrange
            const x = 1.5
            const y = 1.6
            const expected = 3.1

            // Act
            const result = calculator.Add(x, y)

            // Assert
            expect(result).toBeCloseTo(expected)
        })

        test('Given I have 2 positive numbers, When I add them together, Then the commutative property (x + y = y + x) is valid', () => {
            // Arrange
            const x = 5
            const y = 3
            const expected = calculator.Add(y, x)

            // Act
            const result = calculator.Add(x, y)

            // Assert
            expect(result).toBe(expected)
        })
    })

    describe('Subtract tests', () => {
        test('Given I have 2 positive numbers where X > Y, When I subtract them together, Then the subtract of the two numbers is returned', () => {
            // Arrange
            const x = 5
            const y = 3
            const expected = 2

            // Act
            const result = calculator.Subtract(x, y)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 2 positive numbers where X < Y, When I subtract them together, Then the subtract of the two numbers is returned', () => {
            // Arrange
            const x = 2
            const y = 3
            const expected = -1

            // Act
            const result = calculator.Subtract(x, y)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 2 negative numbers, When I subtract them together, Then the subtract of the two numbers is returned', () => {
            // Arrange
            const x = -5
            const y = -3
            const expected = -2

            // Act
            const result = calculator.Subtract(x, y)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 2 zero numbers, When I subtract them together, Then the subtract of the two numbers is returned', () => {
            // Arrange
            const x = 0
            const y = 0
            const expected = 0

            // Act
            const result = calculator.Subtract(x, y)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 2 floating-point numbers, When I subtract them together, Then the subtract of the two numbers is returned', () => {
            // Arrange
            const x = 3.5
            const y = 1.5
            const expected = 2

            // Act
            const result = calculator.Subtract(x, y)

            // Assert
            expect(result).toBeCloseTo(expected)
        })
    })

    describe('Multiply tests', () => {
        test('Given I have 2 positive numbers, When I multiply them together, Then the multiply of the two numbers is returned', () => {
            // Arrange
            const x = 5
            const y = 3
            const expected = 15

            // Act
            const result = calculator.Multiply(x, y)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 2 negative numbers, When I multiply them together, Then the multiply of the two numbers is returned', () => {
            // Arrange
            const x = -5
            const y = -3
            const expected = 15

            // Act
            const result = calculator.Multiply(x, y)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 2 zero numbers, When I multiply them together, Then the multiply of the two numbers is returned', () => {
            // Arrange
            const x = 0
            const y = 0
            const expected = 0

            // Act
            const result = calculator.Multiply(x, y)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 2 floating-point numbers, When I multiply them together, Then the multiply of the two numbers is returned', () => {
            // Arrange
            const x = 1.5
            const y = 1.5
            const expected = 2.25

            // Act
            const result = calculator.Multiply(x, y)

            // Assert
            expect(result).toBeCloseTo(expected)
        })

        test('Given I have 2 positive numbers, When I multiply them together, Then the commutative property (x * y = y * x) is valid', () => {
            // Arrange
            const x = 7
            const y = 3
            const expected = calculator.Multiply(y, x)

            // Act
            const result = calculator.Multiply(x, y)

            // Assert
            expect(result).toBe(expected)
        })
    })

    describe('Divide tests', () => {
        test('Given I have 2 positive numbers, When I divide them together, Then the divide of the two numbers is returned', () => {
            // Arrange
            const dividend = 8
            const divisor = 2
            const expected = 4

            // Act
            const result = calculator.Divide(dividend, divisor)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 2 negative numbers, When I divide them together, Then the divide of the two numbers is returned', () => {
            // Arrange
            const dividend = -6
            const divisor = -3
            const expected = 2

            // Act
            const result = calculator.Divide(dividend, divisor)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 1 positive number and the dividend is zero, When I divide them together, Then the divide of the two numbers is returned', () => {
            // Arrange
            const dividend = 0
            const divisor = 9
            const expected = 0

            // Act
            const result = calculator.Divide(dividend, divisor)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 2 floating-point numbers, When I divide them together, Then the divide of the two numbers is returned', () => {
            // Arrange
            const dividend = 12.5
            const divisor = 6.5
            const expected = 1.92

            // Act
            const result = calculator.Divide(dividend, divisor)

            // Assert
            expect(result).toBeCloseTo(expected)
        })

        test('Given I have 1 positive number and the divisor is zero, When I divide them together, Then divide by zero should throw an error', () => {
            // Arrange
            const dividend = 5
            const divisor = 0

            expect(
                () => calculator.Divide(dividend, divisor) // Act
            ).toThrow('Error: Division by zero') // Assert
        })
    })

    describe('Square root tests', () => {
        test('Given I have 1 positive number, When I calculate the square root of the number, Then the square root of the number is returned', () => {
            // Arrange
            const x = 4
            const expected = 2

            // Act
            const result = calculator.SquareRoot(x)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 1 zero number, When I calculate the square root of the number, Then the square root of the number is returned', () => {
            // Arrange
            const x = 0
            const expected = 0

            // Act
            const result = calculator.SquareRoot(x)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have 1 negative number, When I calculate the square root of the number, Then it should throw an error', () => {
            // Arrange
            const x = -2

            expect(
                () => calculator.SquareRoot(x) // Act
            ).toThrow('Error: Cannot calculate square root of a negative number') // Assert
        })
    })

    describe('Power tests', () => {
        test('Given I have base and exponent positive numbers, When I calculate the power of the base raised to the exponent, Then the result is returned', () => {
            // Arrange
            const base = 4
            const exponent = 2
            const expected = 16

            // Act
            const result = calculator.Power(base, exponent)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have base and exponent negative numbers, When I calculate the power of the base raised to the exponent, Then the result is returned', () => {
            // Arrange
            const base = -5
            const exponent = -3
            const expected = -0.008

            // Act
            const result = calculator.Power(base, exponent)

            // Assert
            expect(result).toBeCloseTo(expected)
        })

        test('Given I have base zero and exponent is greater than zero, When I calculate the power of the base raised to the exponent, Then the result is returned', () => {
            // Arrange
            const base = 0
            const exponent = 5
            const expected = 0

            // Act
            const result = calculator.Power(base, exponent)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have exponent zero and base is greater than zero, When I calculate the power of the base raised to the exponent, Then the result is returned', () => {
            // Arrange
            const base = 5
            const exponent = 0
            const expected = 1

            // Act
            const result = calculator.Power(base, exponent)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have base and exponent floating-point numbers, When I calculate the power of the base raised to the exponent, Then the result is returned', () => {
            // Arrange
            const base = 2.5;
            const exponent = 3.5
            const expected = 24.7052

            // Act
            const result = calculator.Power(base, exponent)

            // Assert
            expect(result).toBeCloseTo(expected)
        })

        test('Given I have large base and exponent positive numbers, When I calculate the power of the base raised to the exponent, Then the result is returned', () => {
            // Arrange
            const base = 10
            const exponent = 10
            const expected = 10_000_000_000

            // Act
            const result = calculator.Power(base, exponent)

            // Assert
            expect(result).toBe(expected)
        })

        test('Given I have large base and exponent negative numbers, When I calculate the power of the base raised to the exponent, Then the result is returned', () => {
            // Arrange
            const base = -10
            const exponent = -10
            const expected = 0.0000000001

            // Act
            const result = calculator.Power(base, exponent)

            // Assert
            expect(result).toBeCloseTo(expected)
        })
    })
})