import { ImageProcessor } from '../../src/week_03_homework/imageProcessor';
import { FileStorageLibrary } from '../../src/week_03_homework/libraries/fileStorageLibrary';
import { ImageProcessingLibrary } from '../../src/week_03_homework/libraries/imageProcessingLibrary';
import { InvalidImageFormatException } from '../../src/week_03_homework/exceptions/InvalidImageFormatException';
import { ProcessingErrorException } from '../../src/week_03_homework/exceptions/ProcessingErrorException';
import { UnknownError } from '../../src/week_03_homework/exceptions/UnknownError';
import { InvalidFilePathException } from '../../src/week_03_homework/exceptions/InvalidFilePathException';
import { mock, mockReset } from 'jest-mock-extended';

const mockedImageProcessingLibrary = mock<ImageProcessingLibrary>();
const mockedFileStorageLibrary = mock<FileStorageLibrary>();

describe('Image processor unit tests', () => {
   let imageProcessor: ImageProcessor;

    beforeEach(() => {
        mockReset(mockedImageProcessingLibrary);
        mockReset(mockedFileStorageLibrary);
        imageProcessor = new ImageProcessor(mockedFileStorageLibrary, mockedImageProcessingLibrary);
    })

    describe('Happy paths with different valid input/output paths on different OS (Windows / Linux)', () => {

        it.each`
            operationSystem | inputFileWithPath | outputFileWithPath
            ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'}
            ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${'D:\\Data\\Images\\outputImage.jpg'}
            ${'Windows'} | ${'D:\\Data\\Images\\inputImage.jpg'} | ${'F:\\Backup\\outputImage.jpg'}
            ${'Linux'} | ${'/home/strmdev/Documents/inputImage.jpg'} | ${'/home/strmdev/Documents/outputImage.jpg'}
            ${'Linux'} | ${'/home/strmdev/Documents/inputImage.jpg'} | ${'/mnt/external/outputImage.jpg'}
            ${'Linux'} | ${'/mnt/external/inputImage.jpg'} | ${'/home/strmdev/Documents/outputImage.jpg'}
        `('Given I have a valid input ($inputFileWithPath) and output ($outputFileWithPath) paths on $operationSystem, When I process and save image, Then the file saved', async ({ inputFileWithPath, outputFileWithPath}) => {
            // Arrange
            const processedContentOfImage = 'processed content of image';
            mockedImageProcessingLibrary.processImage.mockResolvedValue(processedContentOfImage);
            
            // Act
            await imageProcessor.processAndSaveImage(inputFileWithPath, outputFileWithPath);

            // Assert
            expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledTimes(1);
            expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledWith(inputFileWithPath);
            expect(mockedImageProcessingLibrary.processImage(inputFileWithPath)).resolves.toEqual(processedContentOfImage);
            expect(mockedFileStorageLibrary.saveContentIntoFile).toHaveBeenCalledTimes(1);
            expect(mockedFileStorageLibrary.saveContentIntoFile).toHaveBeenCalledWith(processedContentOfImage, outputFileWithPath);
        })

    })

    describe('Error paths', () => {

        describe('We test image processor with different invalid input image types on different OS (Windows / Linux)', () => {
            it.each`
                operationSystem | inputFileWithPath | outputFileWithPath
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.png'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'}
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.bmp'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'}
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.psd'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'}
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.tiff'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'}
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.docx'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.png'} | ${'/home/strmdev/Documents/outputImage.jpg'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.bmp'} | ${'/home/strmdev/Documents/outputImage.jpg'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.psd'} | ${'/home/strmdev/Documents/outputImage.jpg'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.tiff'} | ${'/home/strmdev/Documents/outputImage.jpg'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.txt'} | ${'/home/strmdev/Documents/outputImage.jpg'}
            `('Given I have an invalid input ($inputFileWithPath) and valid output ($outputFileWithPath) paths on $operationSystem, When I process and save image, Then it should throw an error.', async ({ inputFileWithPath, outputFileWithPath}) => {
                // Arrange
                const errorMessage = `Invalid image format: ${inputFileWithPath} Only .jpg images are supported.`;
                const expectedError = new InvalidImageFormatException(errorMessage);
                
                // Act and assert
                await expect(() => imageProcessor.processAndSaveImage(inputFileWithPath, outputFileWithPath)).rejects.toThrow(expectedError);
                expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledTimes(0);
                expect(mockedFileStorageLibrary.saveContentIntoFile).toHaveBeenCalledTimes(0);
            })

        })

        describe('We test image processor with different invalid output image types on different OS (Windows / Linux)', () => {

            it.each`
                operationSystem | inputFileWithPath | outputFileWithPath
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.png'}
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.bmp'}
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.psd'}
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.tiff'}
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.docx'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.jpg'} | ${'/home/strmdev/Documents/outputImage.png'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.jpg'} | ${'/home/strmdev/Documents/outputImage.bmp'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.jpg'} | ${'/home/strmdev/Documents/outputImage.psd'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.jpg'} | ${'/home/strmdev/Documents/outputImage.tiff'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.jpg'} | ${'/home/strmdev/Documents/outputImage.txt'}
            `('Given I have a valid input ($inputFileWithPath) and invalid output ($outputFileWithPath) paths on $operationSystem, When I process and save image, Then it should throw an error.', async ({ inputFileWithPath, outputFileWithPath}) => {
                // Arrange
                const errorMessage = `Invalid image format: ${outputFileWithPath} Only .jpg images are supported.`;
                const expectedError = new InvalidImageFormatException(errorMessage);
                
                // Act and assert
                await expect(() => imageProcessor.processAndSaveImage(inputFileWithPath, outputFileWithPath)).rejects.toThrow(expectedError);
                expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledTimes(0);
                expect(mockedFileStorageLibrary.saveContentIntoFile).toHaveBeenCalledTimes(0);
            })

        })

        describe('We test image processor with different error types in example: large image etc.', () => {

            it.each`
                processingErrorType | inputFileWithPath | outputFileWithPath
                ${'large image size'} | ${'C:\\Users\\strmdev\\Documents\\largeImage.jpg'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'}
                ${'low image resolution'} | ${'C:\\Users\\strmdev\\Documents\\lowImage.jpg'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'}
                ${'unstable or slow internet connection'} | ${'C:\\Users\\strmdev\\Documents\\validImage.jpg'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'}
            `('Given I have a $processingErrorType, When I process image, Then it should throw an error.', async ({ inputFileWithPath, outputFileWithPath, processingErrorType}) => {
                // Arrange
                const errorMessage = `There was an error processing this image: ${processingErrorType}`;
                const expectedError = new ProcessingErrorException(errorMessage);
                mockedImageProcessingLibrary.processImage.mockImplementation(() => { throw expectedError });
                
                // Act and assert
                await expect(() => imageProcessor.processAndSaveImage(inputFileWithPath, outputFileWithPath)).rejects.toThrow(expectedError);
                expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledTimes(1);
                expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledWith(inputFileWithPath);
                expect(mockedFileStorageLibrary.saveContentIntoFile).toHaveBeenCalledTimes(0);
            })

        })

        describe('We test image processor if created an unknown error in the flow', () => {
            
            it.each`
                operationSystem | inputFileWithPath | outputFileWithPath
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.jpg'} | ${'/home/strmdev/Documents/outputImage.jpg'}
            `('Given I have a valid input ($inputFileWithPath) and output ($outputFileWithPath) paths on $operationSystem and encounter an unknown error, When I process and save image, Then it should throw an error.', async ({ inputFileWithPath, outputFileWithPath}) => {
                // Arrange
                const errorMessage = 'Unknown error happened';
                const expectedError = new UnknownError(errorMessage);
                mockedImageProcessingLibrary.processImage.mockImplementation(() => { throw expectedError });
                
                // Act and assert
                await expect(() => imageProcessor.processAndSaveImage(inputFileWithPath, outputFileWithPath)).rejects.toThrow(expectedError);
                expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledTimes(1);
                expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledWith(inputFileWithPath);
                expect(mockedFileStorageLibrary.saveContentIntoFile).toHaveBeenCalledTimes(0);
            })

        })

        describe('We test the save content into file if created an unknown error in the flow', () => {
            
            it.each`
                operationSystem | inputFileWithPath | outputFileWithPath
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.jpg'} | ${'/home/strmdev/Documents/outputImage.jpg'}
            `('Given I have a valid input ($inputFileWithPath) and output ($outputFileWithPath) paths on $operationSystem and encounter an unknown error, When I process and save image, Then it should throw an error.', async ({ inputFileWithPath, outputFileWithPath}) => {
                // Arrange
                const processedContentOfImage = 'processed content of image';
                mockedImageProcessingLibrary.processImage.mockResolvedValue(processedContentOfImage);
    
                const errorMessage = 'Unknown error happened';
                const expectedError = new UnknownError(errorMessage);
                mockedFileStorageLibrary.saveContentIntoFile.mockImplementation(() => { throw expectedError });
                
                // Act and assert
                await expect(() => imageProcessor.processAndSaveImage(inputFileWithPath, outputFileWithPath)).rejects.toThrow(expectedError);
                expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledTimes(1);
                expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledWith(inputFileWithPath);
                expect(mockedFileStorageLibrary.saveContentIntoFile).toHaveBeenCalledTimes(1);
                expect(mockedFileStorageLibrary.saveContentIntoFile).toHaveBeenCalledWith(processedContentOfImage, outputFileWithPath);
            })

        })

        describe('We test special cases with invalid input file paths (nonexistent file, folder etc.)', () => {
            
            it.each`
                operationSystem | inputFileWithPath | outputFileWithPath | filePathErrorMessage
                ${'Windows'} | ${'C:\\Users\\strmdev\\nonexistent\\inputImage.jpg'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'} | ${'The path does not exist'}
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\nonexistent.jpg'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'} | ${'The file does not exist'}
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\.hiddenFile.jpg'} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'} ${'The file is hidden'}
                ${'Linux'} | ${'/home/strmdev/nonexistent/inputImage.jpg'} | ${'/home/strmdev/Documents/outputImage.jpg'} | ${'The path does not exist'}
                ${'Linux'} | ${'/home/strmdev/Documents/nonexistent.jpg'} | ${'/home/strmdev/Documents/outputImage.jpg'} | ${'The file does not exist'}
                ${'Linux'} | ${'/home/strmdev/Documents/.hiddenFile.jpg'} | ${'/home/strmdev/Documents/outputImage.jpg'} | ${'The file is hidden'}
            `('Given I have a special invalid input ($inputFileWithPath) and valid output ($outputFileWithPath) paths on $operationSystem, When I process and save image, Then it should throw an error.', async ({ inputFileWithPath, outputFileWithPath, filePathErrorMessage}) => {
                // Arrange
                const errorMessage = filePathErrorMessage;
                const expectedError = new InvalidFilePathException(errorMessage);
                mockedImageProcessingLibrary.processImage.mockImplementation(() => { throw expectedError });
                
                // Act and assert
                await expect(() => imageProcessor.processAndSaveImage(inputFileWithPath, outputFileWithPath)).rejects.toThrow(expectedError);
                expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledTimes(1);
                expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledWith(inputFileWithPath);
                expect(mockedFileStorageLibrary.saveContentIntoFile).toHaveBeenCalledTimes(0);
            })

        })

        describe('We test special cases with invalid output file paths (nonexistent file, folder etc.)', () => {
            
            it.each`
                operationSystem | inputFileWithPath | outputFileWithPath | filePathErrorMessage
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${'C:\\Users\\strmdev\\nonexistent\\outputImage.jpg'} | ${'The path does not exist'}
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${'C:\\Users\\strmdev\\Documents\\nonexistent.jpg'} | ${'The file does not exist'}
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${'C:\\Users\\strmdev\\Documents\\.hiddenFile.jpg'} ${'The file is hidden'}
                ${'Windows'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${'C:\\Users\\strmdev\\readonlyFolder\\outputImage.jpg'} | ${'The folder is readonly'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.jpg'} | ${'/home/strmdev/nonexistent/outputImage.jpg'} | ${'The path does not exist'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.jpg'} | ${'/home/strmdev/Documents/nonexistent.jpg'} | ${'The file does not exist'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.jpg'} | ${'/home/strmdev/Documents/.hiddenFile.jpg'} | ${'The file is hidden'}
                ${'Linux'} | ${'/home/strmdev/Documents/inputImage.jpg'} | ${'/home/strmdev/readonlyFolder/outputImage.jpg'} | ${'The folder is readonly'}
            `('Given I have a valid input ($inputFileWithPath) and special invalid output ($outputFileWithPath) paths on $operationSystem, When I process and save image, Then it should throw an error.', async ({ inputFileWithPath, outputFileWithPath, filePathErrorMessage}) => {
                // Arrange
                const processedContentOfImage = 'processed content of image';
                mockedImageProcessingLibrary.processImage.mockResolvedValue(processedContentOfImage);

                const errorMessage = filePathErrorMessage;
                const expectedError = new InvalidFilePathException(errorMessage);
                mockedFileStorageLibrary.saveContentIntoFile.mockImplementation(() => { throw expectedError });
                
                // Act and assert
                await expect(() => imageProcessor.processAndSaveImage(inputFileWithPath, outputFileWithPath)).rejects.toThrow(expectedError);
                expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledTimes(1);
                expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledWith(inputFileWithPath);
                expect(mockedFileStorageLibrary.saveContentIntoFile).toHaveBeenCalledTimes(1);
                expect(mockedFileStorageLibrary.saveContentIntoFile).toHaveBeenCalledWith(processedContentOfImage, outputFileWithPath);
            })

        })

        describe('We test the base path validation for input file and output file fields', () => {
            
            it.each`
                params | inputFileWithPath | outputFileWithPath
                ${'an EMPTY input and output paths'} | ${''} | ${''}
                ${'a valid  input and EMPTY output paths'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${''}
                ${'an EMPTY input and valid output paths'} | ${''} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'}
                ${'a NULL input and output paths'} | ${null} | ${null}
                ${'a valid input and a NULL output paths'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${null}
                ${'a NULL input and valid output paths'} | ${null} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'}
                ${'an UNDEFINED input and output paths'} | ${undefined} | ${undefined}
                ${'a valid input and an UNDEFINED output paths'} | ${'C:\\Users\\strmdev\\Documents\\inputImage.jpg'} | ${undefined}
                ${'an UNDEFINED input and valid output paths'} | ${undefined} | ${'C:\\Users\\strmdev\\Documents\\outputImage.jpg'}
            `('Given I have $params, When I process and save image, Then it should throw an error.', async ({ inputFileWithPath, outputFileWithPath}) => {
                // Arrange
                const errorMessage = 'File path is null or undefined or empty string';
                const expectedError = new InvalidFilePathException(errorMessage);
                
                // Act and assert
                await expect(() => imageProcessor.processAndSaveImage(inputFileWithPath, outputFileWithPath)).rejects.toThrow(expectedError);
                expect(mockedImageProcessingLibrary.processImage).toHaveBeenCalledTimes(0);
                expect(mockedFileStorageLibrary.saveContentIntoFile).toHaveBeenCalledTimes(0);
            })

        })

    })
})