"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Calculator = void 0;
class Calculator {
    Add(x, y) {
        return x + y;
    }
    Subtract(x, y) {
        return x - y;
    }
    Multiply(x, y) {
        return x * y;
    }
    Divide(dividend, divisor) {
        if (divisor === 0) {
            throw new Error('Error: Division by zero');
        }
        return dividend / divisor;
    }
    SquareRoot(x) {
        if (x < 0) {
            throw new Error('Error: Cannot calculate square root of a negative number');
        }
        return Math.sqrt(x);
    }
    Power(base, exponent) {
        return Math.pow(base, exponent);
    }
}
exports.Calculator = Calculator;
