"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiscountCalculator = void 0;
const discountCalculatorFactory_1 = require("./factory/discountCalculatorFactory");
class DiscountCalculator {
    calculateDiscountPercentage(level) {
        const discountCalculator = discountCalculatorFactory_1.DiscountCalculatorFactory.createDiscountCalculator(level);
        return discountCalculator ? discountCalculator.calculateDiscountPercentage() : DiscountCalculator.DEFAULT_DISCOUNT_PERCENTAGE;
    }
}
exports.DiscountCalculator = DiscountCalculator;
DiscountCalculator.DEFAULT_DISCOUNT_PERCENTAGE = 0;
