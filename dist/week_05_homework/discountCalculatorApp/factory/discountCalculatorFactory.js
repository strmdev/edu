"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiscountCalculatorFactory = void 0;
const goldDiscountCalculator_1 = require("../modules/levels/goldDiscountCalculator");
const silverDiscountCalculator_1 = require("../modules/levels/silverDiscountCalculator");
const standardDiscountCalculator_1 = require("../modules/levels/standardDiscountCalculator");
class DiscountCalculatorFactory {
    static createDiscountCalculator(level) {
        const discountCalculatorClass = this.discountCalculatorClasses[level];
        return discountCalculatorClass ? new discountCalculatorClass() : undefined;
    }
}
exports.DiscountCalculatorFactory = DiscountCalculatorFactory;
DiscountCalculatorFactory.discountCalculatorClasses = {
    'standard': standardDiscountCalculator_1.StandardDiscountCalculator,
    'silver': silverDiscountCalculator_1.SilverDiscountCalculator,
    'gold': goldDiscountCalculator_1.GoldDiscountCalculator,
};
