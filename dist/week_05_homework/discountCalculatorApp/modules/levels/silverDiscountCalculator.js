"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SilverDiscountCalculator = void 0;
const abstractDiscountCalculator_1 = require("./base/abstractDiscountCalculator");
class SilverDiscountCalculator extends abstractDiscountCalculator_1.AbstractDiscountCalculator {
    calculateDiscountPercentage() {
        return SilverDiscountCalculator.SILVER_DISCOUNT_PERCENTAGE;
    }
}
exports.SilverDiscountCalculator = SilverDiscountCalculator;
SilverDiscountCalculator.SILVER_DISCOUNT_PERCENTAGE = 10;
