"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StandardDiscountCalculator = void 0;
const abstractDiscountCalculator_1 = require("./base/abstractDiscountCalculator");
class StandardDiscountCalculator extends abstractDiscountCalculator_1.AbstractDiscountCalculator {
    calculateDiscountPercentage() {
        return StandardDiscountCalculator.STANDARD_DISCOUNT_PERCENTAGE;
    }
}
exports.StandardDiscountCalculator = StandardDiscountCalculator;
StandardDiscountCalculator.STANDARD_DISCOUNT_PERCENTAGE = 5;
