"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GoldDiscountCalculator = void 0;
const abstractDiscountCalculator_1 = require("./base/abstractDiscountCalculator");
class GoldDiscountCalculator extends abstractDiscountCalculator_1.AbstractDiscountCalculator {
    calculateDiscountPercentage() {
        return GoldDiscountCalculator.GOLD_DISCOUNT_PERCENTAGE + GoldDiscountCalculator.EXTRA_DISCOUNT_PERCENTAGE;
    }
}
exports.GoldDiscountCalculator = GoldDiscountCalculator;
GoldDiscountCalculator.GOLD_DISCOUNT_PERCENTAGE = 15;
GoldDiscountCalculator.EXTRA_DISCOUNT_PERCENTAGE = 5;
