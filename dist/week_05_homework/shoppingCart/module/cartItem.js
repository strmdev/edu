"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CartItem = void 0;
class CartItem {
    constructor(product, price) {
        this.product = product;
        this.price = price;
    }
    setProduct(product) {
        this.product = product;
    }
    getProduct() {
        return this.product;
    }
    setPrice(price) {
        this.price = price;
    }
    getPrice() {
        return this.price;
    }
}
exports.CartItem = CartItem;
