"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ShoppingCart {
    constructor(items = []) {
        this.items = items;
    }
    addItem(item) {
        this.items.push(item);
    }
    calculateTotal() {
        let total = 0;
        for (const item of this.items) {
            total += item.getPrice();
        }
        return total;
    }
}
