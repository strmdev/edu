"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmployeeManagementSystem = void 0;
class EmployeeManagementSystem {
    constructor(employees = [], payrollCalculatorInterface, employeeReportGeneratorInterface, promoteEmployeeInterface) {
        this.employees = employees;
        this.payrollCalculatorInterface = payrollCalculatorInterface;
        this.employeeReportGeneratorInterface = employeeReportGeneratorInterface;
        this.promoteEmployeeInterface = promoteEmployeeInterface;
    }
    addEmployee(employee) {
        this.employees.push(employee);
    }
    calculatePayroll() {
        return this.payrollCalculatorInterface.calculatePayroll(this.employees);
    }
    generateReports() {
        return this.employeeReportGeneratorInterface.generateReports(this.employees);
    }
    promoteEmployee(employee) {
        this.promoteEmployeeInterface.promoteEmployee(employee);
    }
}
exports.EmployeeManagementSystem = EmployeeManagementSystem;
