"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PayrollCalculator = void 0;
class PayrollCalculator {
    calculatePayroll(employees) {
        let totalPayroll = 0;
        for (const employee of employees) {
            totalPayroll += employee.calculateSalary();
        }
        return totalPayroll;
    }
}
exports.PayrollCalculator = PayrollCalculator;
