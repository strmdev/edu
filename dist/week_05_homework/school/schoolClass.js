"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SchoolClass = void 0;
class SchoolClass {
    constructor(students = []) {
        this.students = students;
    }
    addStudent(student) {
        this.students.push(student);
    }
    getStudentCount() {
        return this.students.length;
    }
}
exports.SchoolClass = SchoolClass;
