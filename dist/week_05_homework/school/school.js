"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.School = void 0;
class School {
    constructor(classes = []) {
        this.classes = classes;
    }
    addClass(schoolClass) {
        this.classes.push(schoolClass);
    }
    getStudentCount() {
        let totalStudents = 0;
        for (const schoolClass of this.classes) {
            totalStudents += schoolClass.getStudentCount();
        }
        return totalStudents;
    }
}
exports.School = School;
