"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserInputValidator = void 0;
class UserInputValidator {
    validateUserInput(input) {
        return (input.length >= 5 && input.length <= 20) && (/^[a-zA-Z0-9]+$/.test(input));
    }
}
exports.UserInputValidator = UserInputValidator;
