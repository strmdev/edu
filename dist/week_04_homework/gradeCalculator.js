"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GradeCalculator = void 0;
class GradeCalculator {
    calculateGrade(score) {
        return score >= 90 ? 'A' : score >= 80 ? 'B' : score >= 70 ? 'C' : 'D';
    }
}
exports.GradeCalculator = GradeCalculator;
