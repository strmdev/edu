"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PremiumCampaignData = void 0;
class PremiumCampaignData {
    constructor(isPremiumUser, emailsOfActiveUsersInCampaign, isSentCoupon, searchedEmail, countOfCoupon) {
        this.isPremiumUser = isPremiumUser;
        this.emailsOfActiveUsersInCampaign = emailsOfActiveUsersInCampaign;
        this.isSentCoupon = isSentCoupon;
        this.searchedEmail = searchedEmail;
        this.countOfCoupon = countOfCoupon;
    }
    isPremiumSubscriber() {
        return this.isPremiumUser === true;
    }
    getEmailsOfActiveUsersInCampaign() {
        return this.emailsOfActiveUsersInCampaign;
    }
    hasSentCoupon() {
        return this.isSentCoupon === true;
    }
    getSearchedEmail() {
        return this.searchedEmail;
    }
    getCountOfCoupon() {
        return this.countOfCoupon;
    }
}
exports.PremiumCampaignData = PremiumCampaignData;
