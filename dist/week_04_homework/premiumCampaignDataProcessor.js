"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PremiumCampaignDataProcessor = void 0;
class PremiumCampaignDataProcessor {
    processPremiumCampaignData(premiumCampaignData) {
        return !premiumCampaignData.isPremiumSubscriber() ? 'No action taken.' : premiumCampaignData.hasSentCoupon() ? this.getActivatedCouponsInfo(premiumCampaignData) : this.compileCoupons(premiumCampaignData);
    }
    getActivatedCouponsInfo(premiumCampaignData) {
        const searchedEmail = premiumCampaignData.getSearchedEmail();
        const usersWithActivatedCoupons = premiumCampaignData.getEmailsOfActiveUsersInCampaign();
        return usersWithActivatedCoupons.indexOf(searchedEmail) !== -1 ? `User found: ${searchedEmail}` : `User (${searchedEmail}) has not activated any coupons.`;
    }
    compileCoupons(premiumCampaignData) {
        const countOfCoupon = premiumCampaignData.getCountOfCoupon();
        const couponProcessingMessage = (count) => `Create ${count}. coupon...`;
        return Array.from({ length: countOfCoupon }, (_, indexOfCoupon) => couponProcessingMessage(indexOfCoupon + 1)).join('');
    }
}
exports.PremiumCampaignDataProcessor = PremiumCampaignDataProcessor;
