"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Lecturer = void 0;
const person_1 = require("./person");
class Lecturer extends person_1.Person {
    constructor(name, emailAddress, phoneNumber) {
        super(name, emailAddress, phoneNumber);
        this.assignedCourses = [];
    }
    assignToCourse(course) {
        this.assignedCourses.push(course);
    }
    getAssignedCourses() {
        return this.assignedCourses;
    }
}
exports.Lecturer = Lecturer;
