"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Course = void 0;
class Course {
    constructor(courseName, startDate, lengthInWeeks, costInHuf) {
        this.courseName = courseName;
        this.startDate = startDate;
        this.lengthInWeeks = lengthInWeeks;
        this.costInHuf = costInHuf;
        this.students = [];
    }
    getCourseName() {
        return this.courseName;
    }
    getStartDate() {
        return this.startDate;
    }
    getLengthInWeeks() {
        return this.lengthInWeeks;
    }
    getCostInHuf() {
        return this.costInHuf;
    }
    getStudentCount() {
        return this.students.length;
    }
    addStudent(student) {
        this.students.push(student);
    }
    getStudents() {
        return this.students;
    }
}
exports.Course = Course;
