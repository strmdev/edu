"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Person = void 0;
class Person {
    constructor(name, emailAddress, phoneNumber) {
        this.name = name;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
    }
    getName() {
        return this.name;
    }
    getEmailAddress() {
        return this.emailAddress;
    }
    getPhoneNumber() {
        return this.phoneNumber;
    }
}
exports.Person = Person;
