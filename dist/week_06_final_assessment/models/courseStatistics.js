"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CourseStatistics = void 0;
class CourseStatistics {
    constructor(courseName, totalLectures, lecturesCompleted, progress, lastAccessed) {
        this.courseName = courseName;
        this.totalLectures = totalLectures;
        this.lecturesCompleted = lecturesCompleted;
        this.progress = progress;
        this.lastAccessed = lastAccessed;
    }
    getCourseName() {
        return this.courseName;
    }
    getTotalLectures() {
        return this.totalLectures;
    }
    getLecturesCompleted() {
        return this.lecturesCompleted;
    }
    getProgress() {
        return this.progress;
    }
    getLastAccessed() {
        return this.lastAccessed;
    }
}
exports.CourseStatistics = CourseStatistics;
