"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Student = void 0;
const person_1 = require("./person");
class Student extends person_1.Person {
    constructor(name, emailAddress, phoneNumber, registeredCourses = []) {
        super(name, emailAddress, phoneNumber);
        this.registeredCourses = registeredCourses;
    }
    getRegisteredCourses() {
        return this.registeredCourses;
    }
}
exports.Student = Student;
