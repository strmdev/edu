"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.delay = void 0;
function delay(milisec) {
    return new Promise((resolve) => {
        setTimeout(resolve, milisec);
    });
}
exports.delay = delay;
