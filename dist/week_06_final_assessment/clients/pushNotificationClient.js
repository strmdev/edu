"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PushNotificationClient = void 0;
const common_1 = require("./common");
class PushNotificationClient {
    constructor(deviceToken) {
        this.deviceToken = deviceToken;
    }
    sendNotification(message) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(`Sending push notification to ${this.deviceToken}: ${message}`);
            yield (0, common_1.delay)(1500);
        });
    }
}
exports.PushNotificationClient = PushNotificationClient;
