"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DatabaseClient = void 0;
const course_1 = require("../models/course");
const courseStatistics_1 = require("../models/courseStatistics");
const common_1 = require("./common");
class DatabaseClient {
    insertCourse(course) {
        return __awaiter(this, void 0, void 0, function* () {
            yield (0, common_1.delay)(1500);
        });
    }
    insertStudentToCourse(student, courseName) {
        return __awaiter(this, void 0, void 0, function* () {
            yield (0, common_1.delay)(1500);
        });
    }
    selectCourseByName(courseName) {
        return __awaiter(this, void 0, void 0, function* () {
            const cleanCodeCourse = new course_1.Course('Clean Code', new Date(2024, 6, 1), 6, 100000);
            yield (0, common_1.delay)(1500);
            return cleanCodeCourse;
        });
    }
    selectAllCourses() {
        return __awaiter(this, void 0, void 0, function* () {
            const cleanCodeCourse = new course_1.Course('Clean Code', new Date(2024, 6, 1), 6, 100000);
            const unitTestingCourse = new course_1.Course('Unit testing', new Date(2024, 7, 1), 7, 200000);
            yield (0, common_1.delay)(1500);
            return [cleanCodeCourse, unitTestingCourse];
        });
    }
    selectCourseStatisticsByName(courseName) {
        return __awaiter(this, void 0, void 0, function* () {
            const cleanCodeCourseStatistic = new courseStatistics_1.CourseStatistics(courseName, 60, 30, 50, new Date(2024, 4, 29));
            yield (0, common_1.delay)(1500);
            return cleanCodeCourseStatistic;
        });
    }
}
exports.DatabaseClient = DatabaseClient;
