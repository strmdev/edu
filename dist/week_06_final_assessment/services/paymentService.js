"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentService = void 0;
const networkError_1 = require("../exceptions/networkError");
const resourceNotFoundError_1 = require("../exceptions/resourceNotFoundError");
const unknownError_1 = require("../exceptions/unknownError");
class PaymentService {
    constructor(financialApiClient) {
        this.financialApiClient = financialApiClient;
    }
    getIsOrderPayed(student) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.financialApiClient.checkPaymentStatus(student);
            }
            catch (error) {
                if (error instanceof networkError_1.NetworkError || error instanceof resourceNotFoundError_1.ResourceNotFoundError) {
                    throw error;
                }
                throw new unknownError_1.UnknownError(PaymentService.DEFAULT_ERROR_MESSAGE, error);
            }
        });
    }
    makePaymentForCourse(student) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.financialApiClient.makePayment(student);
            }
            catch (error) {
                if (error instanceof networkError_1.NetworkError || error instanceof resourceNotFoundError_1.ResourceNotFoundError) {
                    throw error;
                }
                throw new unknownError_1.UnknownError(PaymentService.DEFAULT_ERROR_MESSAGE, error);
            }
        });
    }
}
exports.PaymentService = PaymentService;
PaymentService.DEFAULT_ERROR_MESSAGE = 'Unknown error happened.';
