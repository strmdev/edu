"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CourseService = void 0;
const validationError_1 = require("../exceptions/validationError");
class CourseService {
    constructor(courseRepository, paymentService, notificationService) {
        this.courseRepository = courseRepository;
        this.paymentService = paymentService;
        this.notificationService = notificationService;
    }
    addCourse(course) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.courseRepository.addCourse(course);
        });
    }
    getCourses() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.courseRepository.getCourses();
        });
    }
    addStudentToCourse(student, courseName) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.validateCourseExists(courseName);
            yield this.validatePaymentStatus(student);
            yield this.courseRepository.addStudentToCourse(student, courseName);
            const notificationMessage = `${student.getName()} student was added to course.`;
            yield this.notificationService.sendNotifications(notificationMessage);
        });
    }
    getCourseStatistics(courseName) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.validateCourseExists(courseName);
            return yield this.courseRepository.getCourseStatistics(courseName);
        });
    }
    validateCourseExists(courseName) {
        return __awaiter(this, void 0, void 0, function* () {
            const course = yield this.courseRepository.getCourseByName(courseName);
            if (!course) {
                const errorMessage = 'Course not found';
                throw new validationError_1.ValidationError(errorMessage);
            }
        });
    }
    validatePaymentStatus(student) {
        return __awaiter(this, void 0, void 0, function* () {
            const isCoursePayedByStudent = yield this.paymentService.getIsOrderPayed(student);
            if (!isCoursePayedByStudent) {
                const errorMessage = 'Course is not yet paid by Student.';
                throw new validationError_1.ValidationError(errorMessage);
            }
        });
    }
}
exports.CourseService = CourseService;
