"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CourseRepository = void 0;
const networkError_1 = require("../exceptions/networkError");
const unknownError_1 = require("../exceptions/unknownError");
class CourseRepository {
    constructor(databaseClient) {
        this.databaseClient = databaseClient;
    }
    addCourse(course) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.databaseClient.insertCourse(course);
            }
            catch (error) {
                if (error instanceof networkError_1.NetworkError) {
                    throw error;
                }
                throw new unknownError_1.UnknownError(CourseRepository.ERROR_MESSAGE_DB_CLIENT_FAILED, error);
            }
        });
    }
    addStudentToCourse(student, courseName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.databaseClient.insertStudentToCourse(student, courseName);
            }
            catch (error) {
                if (error instanceof networkError_1.NetworkError) {
                    throw error;
                }
                throw new unknownError_1.UnknownError(CourseRepository.ERROR_MESSAGE_DB_CLIENT_FAILED, error);
            }
        });
    }
    getCourseByName(courseName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.databaseClient.selectCourseByName(courseName);
            }
            catch (error) {
                if (error instanceof networkError_1.NetworkError) {
                    throw error;
                }
                throw new unknownError_1.UnknownError(CourseRepository.ERROR_MESSAGE_DB_CLIENT_FAILED, error);
            }
        });
    }
    getCourses() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.databaseClient.selectAllCourses();
            }
            catch (error) {
                if (error instanceof networkError_1.NetworkError) {
                    throw error;
                }
                throw new unknownError_1.UnknownError(CourseRepository.ERROR_MESSAGE_DB_CLIENT_FAILED, error);
            }
        });
    }
    getCourseStatistics(courseName) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.databaseClient.selectCourseStatisticsByName(courseName);
            }
            catch (error) {
                if (error instanceof networkError_1.NetworkError) {
                    throw error;
                }
                throw new unknownError_1.UnknownError(CourseRepository.ERROR_MESSAGE_DB_CLIENT_FAILED, error);
            }
        });
    }
}
exports.CourseRepository = CourseRepository;
CourseRepository.ERROR_MESSAGE_DB_CLIENT_FAILED = 'DbClient failed.';
