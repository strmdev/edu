"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnknownError = void 0;
const logger_1 = require("./logger");
class UnknownError extends Error {
    constructor(message, error) {
        super(message);
        this.error = error;
        this.name = this.constructor.name;
        logger_1.Logger.logError(message, this.error);
    }
}
exports.UnknownError = UnknownError;
