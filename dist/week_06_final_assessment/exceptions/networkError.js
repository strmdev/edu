"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NetworkError = void 0;
class NetworkError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}
exports.NetworkError = NetworkError;
