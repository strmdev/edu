"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logger = void 0;
class Logger {
    static logError(errorMessage, errorObject) {
        console.log(errorMessage);
        if (errorObject) {
            console.log(JSON.stringify(errorObject));
        }
    }
}
exports.Logger = Logger;
