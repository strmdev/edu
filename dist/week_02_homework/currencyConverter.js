"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrencyConverter = void 0;
const ResourceNotFoundError_1 = require("./exceptions/ResourceNotFoundError");
const UnknownError_1 = require("./exceptions/UnknownError");
const ValidationError_1 = require("./exceptions/ValidationError");
class CurrencyConverter {
    constructor(exchangeRateService) {
        this.exchangeRateService = exchangeRateService;
        this.FIXED_AMOUNT = 100;
    }
    Convert(amount, fromCurrency, toCurrency) {
        this.validateAmount(amount);
        const exchangeRate = this.getExchangeRate(fromCurrency, toCurrency);
        this.validateExchangeRate(exchangeRate);
        return amount * exchangeRate;
    }
    GenerateConversionReport(fromCurrency, toCurrency, startDate, endDate) {
        const conversions = [];
        const currentDate = new Date(startDate);
        while (currentDate <= endDate) {
            const exchangeRate = this.getExchangeRate(fromCurrency, toCurrency);
            this.validateExchangeRate(exchangeRate);
            this.calculateConversion(exchangeRate, conversions, currentDate);
        }
        return `Conversion Report:\n${conversions.join('\n')}`;
    }
    getExchangeRate(fromCurrency, toCurrency) {
        try {
            return this.exchangeRateService.getExchangeRate(fromCurrency, toCurrency);
        }
        catch (error) {
            if (error instanceof ResourceNotFoundError_1.ResourceNotFoundError) {
                throw error;
            }
            throw new UnknownError_1.UnknownError('Unknown error happened.');
        }
    }
    calculateConversion(exchangeRate, conversions, currentDate) {
        const convertedAmount = this.FIXED_AMOUNT * exchangeRate; // Assume a fixed amount for simplicity
        conversions.push(convertedAmount);
        currentDate.setDate(currentDate.getDate() + 1);
    }
    validateExchangeRate(exchangeRate) {
        if (isNaN(exchangeRate)) {
            throw new ValidationError_1.ValidationError('Invalid exchange rate.');
        }
        if (!exchangeRate) {
            throw new ValidationError_1.ValidationError('Unable to fetch exchange rate.');
        }
    }
    validateAmount(amount) {
        if (isNaN(amount)) {
            throw new ValidationError_1.ValidationError('Invalid amount input.');
        }
    }
}
exports.CurrencyConverter = CurrencyConverter;
