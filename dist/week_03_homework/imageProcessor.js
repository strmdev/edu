"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ImageProcessor = void 0;
const InvalidImageFormatException_1 = require("./exceptions/InvalidImageFormatException");
const ProcessingErrorException_1 = require("./exceptions/ProcessingErrorException");
const UnknownError_1 = require("./exceptions/UnknownError");
const InvalidFilePathException_1 = require("./exceptions/InvalidFilePathException");
class ImageProcessor {
    constructor(fileStorageLibrary, imageProcessingLibrary) {
        this.fileStorageLibrary = fileStorageLibrary;
        this.imageProcessingLibrary = imageProcessingLibrary;
    }
    processAndSaveImage(inputFileWithPath, outputFileWithPath) {
        return __awaiter(this, void 0, void 0, function* () {
            this.validateFilePathNullOrEmpty(inputFileWithPath);
            this.validateFilePathNullOrEmpty(outputFileWithPath);
            this.validateImageFileFormat(inputFileWithPath);
            this.validateImageFileFormat(outputFileWithPath);
            const processedContentOfImage = yield this.processImage(inputFileWithPath);
            yield this.saveProcessedImageContentIntoFile(processedContentOfImage, outputFileWithPath);
        });
    }
    validateFilePathNullOrEmpty(fileWithPath) {
        const isfileWithPathNullOrUndefinedOrEmpty = fileWithPath == null || fileWithPath.trim() === '';
        if (isfileWithPathNullOrUndefinedOrEmpty) {
            const errorMessage = 'File path is null or undefined or empty string';
            throw new InvalidFilePathException_1.InvalidFilePathException(errorMessage);
        }
    }
    validateImageFileFormat(fileWithPath) {
        if (!fileWithPath.endsWith(ImageProcessor.EXPECTED_FILE_FORMAT)) {
            const errorMessage = `Invalid image format: ${fileWithPath} Only ${ImageProcessor.EXPECTED_FILE_FORMAT} images are supported.`;
            throw new InvalidImageFormatException_1.InvalidImageFormatException(errorMessage);
        }
    }
    processImage(inputFileWithPath) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.imageProcessingLibrary.processImage(inputFileWithPath);
            }
            catch (error) {
                if (error instanceof ProcessingErrorException_1.ProcessingErrorException || error instanceof InvalidFilePathException_1.InvalidFilePathException) {
                    throw error;
                }
                const errorMessage = 'Unknown error happened';
                throw new UnknownError_1.UnknownError(errorMessage, error);
            }
        });
    }
    saveProcessedImageContentIntoFile(processedContentOfImage, outputFileWithPath) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                this.validateImageFileFormat(outputFileWithPath);
                yield this.fileStorageLibrary.saveContentIntoFile(processedContentOfImage, outputFileWithPath);
            }
            catch (error) {
                if (error instanceof InvalidImageFormatException_1.InvalidImageFormatException || error instanceof InvalidFilePathException_1.InvalidFilePathException) {
                    throw error;
                }
                const errorMessage = 'Unknown error happened';
                throw new UnknownError_1.UnknownError(errorMessage, error);
            }
        });
    }
}
exports.ImageProcessor = ImageProcessor;
ImageProcessor.EXPECTED_FILE_FORMAT = '.jpg';
