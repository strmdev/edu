"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvalidImageException = void 0;
class InvalidImageException extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}
exports.InvalidImageException = InvalidImageException;
