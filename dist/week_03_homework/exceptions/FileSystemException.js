"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FileSystemException = void 0;
class FileSystemException extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}
exports.FileSystemException = FileSystemException;
