"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvalidImageFormatException = void 0;
class InvalidImageFormatException extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}
exports.InvalidImageFormatException = InvalidImageFormatException;
