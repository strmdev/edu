"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InvalidFilePathException = void 0;
class InvalidFilePathException extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}
exports.InvalidFilePathException = InvalidFilePathException;
