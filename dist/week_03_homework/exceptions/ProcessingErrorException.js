"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProcessingErrorException = void 0;
class ProcessingErrorException extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}
exports.ProcessingErrorException = ProcessingErrorException;
